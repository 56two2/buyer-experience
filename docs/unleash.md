# GitLab + Unleash feature flags

GitLab's own feature flag solution uses unleash components (proxy + frontend client) to manipulate feature flags or toggles.

## Installation

### Unleash Vue Client

Run `unleash-proxy-client` to install the necessary package for the client.

## Usage

### Create a feature toggle on GitLab

In the GitLab's interface, go to Buyer Experience repo, then go to Deployments, the Feature Flags.

1) On the top bar, select `Menu > Projects` and find the Buyer Experience project.
2) On the left sidebar, select `Deployments > Feature Flags`.
3) Select `New Feature Flag`.
4) Enter a name that contains only lowercase letters, digits, '_' and '-'. Must start with a letter, and cannot end with '-' or '_'

For more information please go to the [official feature flag docs on creating a new feature flag.](https://docs.gitlab.com/ee/operations/feature_flags.html#create-a-feature-flag)

### Run proxy

Go to unleash folder by running `cd /unleash` and then run `docker compose up`
to start the proxy server. Verify that toggles are being listed as a response of  running `curl http://localhost:3000/proxy -H "Authorization: client-proxy-secret"` in your console.

You need to change the `UNLEASH_PROXY_SECRETS` proxy variable, and update this change in `curl http://localhost:3000/proxy -H "Authorization: <YOUR_NEW_SECRET_HERE>"`

### Import unleash helper

In the component or page that you want to use a toggle, import the unleash helper as a relative import.

```vue

import { unleash } from '~/unleash';

```

#### Create boolean variable for an active toggle

There should a boolean variable that tracks whether the feature toggle is enabled or disabled from the GitLab's interface.
data
mounted

```vue

data() {
    return {
        isFlagEnabled: false,
    };
},

mounted() {
    this.isFlagEnabled = unleash.isEnabled('test');
},


```

Then, when the component (or page) is mounted, we can assign the value of the toggle to the boolean tracking it in the component.

#### Use boolean variable for a component variation

In this way, you can then use the boolean variable to conditionally render different content on the compiled markup.

```vue

  <div v-if="isFlagEnabled">Example div for feature flag</div>

```

## Configuration

### Configure proxy variables

1) On the top bar, select `Menu > Projects` and find the Buyer Experience project.
2) On the left sidebar, select `Deployments > Feature Flags`.
3) Select `Configure` to view the following:

- API URL: URL where the client (application) connects to get a list of feature flags.

- Instance ID: Unique token that authorizes the retrieval of the feature flags.

- Application name: The name of the environment the application runs in (not the name of the application itself).

Unleash proxy needs the following variables to be able to work properly:

- UNLEASH_URL: It's the API URL set by GitLab's project.

- UNLEASH_INSTANCE_ID: It's the Instance IDL set by GitLab's project.

- UNLEASH_APP_NAME: It's the name of the environment your application runs in.

- UNLEASH_PROXY_SECRETS: It's the secret used to authenticate the client to the proxy.

- UNLEASH_API_TOKEN: Required variable that can be set to any arbitrary value (not used because we're using GitLab's infrastructure to run the API)

To change these variables please follow the instructions on [how to access credentials](https://docs.gitlab.com/ee/operations/feature_flags.html#get-access-credentials) and adjust the `/unleash/docker-compose.yml` file accordingly.
