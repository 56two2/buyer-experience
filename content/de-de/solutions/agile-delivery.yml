---
  title: Agile Planung mit GitLab
  description: Anleitung zum Verwenden von GitLab als agiles Projektmanagement-Tool für agile Prozesse wie Scrum, Kanban und Scaled Agile Framework (SAFe).
  report_cta:
    layout: "dark"
    title: Analystenberichte
    reports:
    - description: "GitLab als einziger Marktführer in The Forrester Wave™ anerkannt: Integrierte Software-Entwicklungsplattformen, Q2 2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      link_text: Lesen Sie den Bericht
    - description: "GitLab als Marktführer im Gartner® Magic Quadrant 2023™ für DevOps-Plattformen anerkannt"
      url: /gartner-magic-quadrant/
      link_text: Lesen Sie den Bericht
  components:
    - name: 'solutions-hero'
      data:
        title: Agile Planung
        subtitle: Plane und verwalte deine Projekte, Programme und Produkte mit integrierter Agile-Unterstützung
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Kostenlos testen
          url: /free-trial/
        secondary_btn:
          text: Erfahre mehr über die Preise
          url: /pricing/
          data_ga_name: Learn about pricing
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Bild: GitLab für den öffentlichen Sektor"
    - name: 'copy-media'
      data:
        block:
          - header: Agile Planung mit GitLab
            text: |
              Entwicklungsteams beschleunigen die Wertschöpfung mit iterativen, schrittweisen und schlanken Projektmethoden wie Scrum, Kanban und Extreme Programming (XP). Große Unternehmen haben Agile im Unternehmensmaßstab durch eine Vielzahl von Frameworks eingeführt, darunter Scaled Agile Framework (SAFe), Spotify und Large Scale Scrum (LeSS). Mit GitLab können Teams Agile Praktiken und Prinzipien anwenden, um ihre Arbeit zu organisieren und zu verwalten, unabhängig von der gewählten Methodik.
            link_href: /demo/
            link_text: Mehr erfahren
            video:
              video_url: https://www.youtube.com/embed/wmtZKC8m2ew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: Vorteile von GitLab
            text: |
              Als umfassendste DevSecOps-Plattform ist GitLab:

              *   **Nahtlos:** GitLab unterstützt die Zusammenarbeit und Sichtbarkeit für Agile Teams – von der Planung bis zur Bereitstellung und darüber hinaus – mit einem einzigen Benutzererlebnis und einem gemeinsamen Toolset
              *   **Integriert:** Verwalte Projekte in demselben System, in dem du deine Arbeit erledigst
              *   **Skalierbar:** Organisiere mehrere Agile Teams, um eine unternehmensweite agile Skalierbarkeit zu erreichen
              *   **Flexibel:** Passe Standardfunktionen an die Anforderungen deiner Methodik an, egal ob du deine eigene Variante von Agile einführst oder ein formales Framework übernimmst
              *   **Einfach zu erlernen:** Schau dir unsere [Schnellstart](https://www.youtube.com/watch?v=VR2r1TJCDew){data-ga-name="quick start" data-ga-location="body"}-Anleitung zum Aufbau von Agile Teams an
            image:
              image_url: /nuxt-images/blogimages/cicd_pipeline_infograph.png
              alt: ""
          - header: Agile Projekte verwalten
            inverted: true
            text: |
              GitLab ermöglicht schlankes und agiles Projektmanagement, von der einfachen Ticket-Verfolgung bis hin zu Scrum- und Kanban-ähnlichem Projektmanagement. Ganz gleich, ob du nur ein paar Tickets verfolgst oder den kompletten DevSecOps-Lebenszyklus eines Entwicklerteams verwaltest, mit GitLab ist dein Team bestens ausgestattet.

              *   **Planen, zuweisen und verfolgen** mit [Tickets](https://docs.gitlab.com/ee/user/project/issues/index.html){data-ga-name="issues" data-ga-location="body"}
              *   **Arbeit organisieren** mit [Labels](https://docs.gitlab.com/ee/user/project/labels.html){data-ga-name="labels" data-ga-location="body"}, [iterations](https://docs.gitlab.com/ee/user/group/iterations/){data-ga-name="iterations" data-ga-location="body"} und [Meilensteinen](https://docs.gitlab.com/ee/user/project/milestones/index.html#overview){data-ga-name="milestones" data-ga-location="body"}
              *   **Arbeit visualisieren** mit [Boards](https://docs.gitlab.com/ee/user/project/issue_board.html#issue-boards){data-ga-name="boards" data-ga-location="body"}
              *   **Arbeit mit dem Output abgleichen** mit [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/index.html){data-ga-name="merge requests" data-ga-location="body"}
            video:
              video_url: https://www.youtube.com/embed/wmtZKC8m2ew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: Verwalten von Programmen und Portfolios
            text: |
              Behalte den Überblick und die Kontrolle über die Teammitglieder und Projekte, die mit den Geschäftsinitiativen verbunden sind. Lege Richtlinien und Berechtigungen fest und setze sie durch, verfolge den Fortschritt und die Geschwindigkeit mehrerer Projekte und Gruppen und setze Prioritäten, um den größtmöglichen Nutzen zu erzielen.

              *   **Organisiere** neue Geschäftsinitiativen und -bemühungen in [Epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="epics" data-ga-location="body"}
              *   **Baue Teams** und Projekte zu Programmen aus – ohne Abstriche bei der Sicherheit und Sichtbarkeit – und verwende dabei Untergruppen
              *   **Plane** [Sub-Epics](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#multi-level-child-epics){data-ga-name="sub-epics" data-ga-location="body"} und Tickets in Iterationen und Meilensteinen
              *   **Visualisiere** die Wertschöpfung mit Roadmaps, [Insights](https://docs.gitlab.com/ee/user/project/insights/){data-ga-name="insights" data-ga-location="body"}, und [Wertstromanalyse](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html){data-ga-name="value stream analytics" data-ga-location="body"}
            video:
              video_url: https://www.youtube.com/embed/VR2r1TJCDew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: Scaled Agile Framework (SAFe) mit GitLab
            inverted: true
            text: |
              Erfahre, wie deine Organisation mit GitLab ein Framework nach dem Scaled Agile Framework (SAFe) aufbauen kann. Informiere dich über die Details zum Aufbau deines Agile Frameworks für Entwicklungsteams, das auf drei Säulen beruht: Team, Programm und Portfolio.
            video:
              video_url: https://www.youtube.com/embed/PmFFlTH2DQk?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: copy-media
      data:
        block:
          - header: Funktionen
            miscellaneous: |
              *   **Tickets:** Beginne mit einem [Ticket](https://docs.gitlab.com/ee/user/project/issues/){data-ga-name="start with an issue" data-ga-location="body"}, das eine einzelne Funktion erfasst, die den Nutzern einen geschäftlichen Nutzen bringt.
              *   **Aufgaben:** Oft wird eine User Story weiter in einzelne Aufgaben unterteilt. Du kannst eine [Aufgabenliste](https://docs.gitlab.com/ee/user/markdown.html#task-lists){data-ga-name="task list" data-ga-location="body"} innerhalb der Beschreibung eines Tickets in GitLab erstellen, um diese einzelnen Aufgaben weiter zu kennzeichnen.
              * **Ticketübersicht:** Alles ist an einem Ort. [Verfolge](https://docs.gitlab.com/ee/user/project/issues/#issues-per-project){data-ga-name="track" data-ga-location="body"} Tickets und kommuniziere den Fortschritt, ohne zwischen Produkten wechseln zu müssen. Eine Schnittstelle, um deine Tickets vom Backlog bis zur Fertigstellung zu verfolgen.
              *   **Epics:** Verwalte dein Projektportfolio effizienter und mit weniger Aufwand, indem du mit [Epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="milestones with epics" data-ga-location="body"} Gruppen von Tickets, die ein gemeinsames Thema haben, projekt- und meilensteinübergreifend verfolgst.
              *   **Meilensteine:** Verfolge Tickets und Merge Requests, die erstellt wurden, um ein größeres Ziel in einer bestimmten Zeitspanne zu erreichen, mit GitLab [Meilensteine](https://docs.gitlab.com/ee/user/project/milestones/){data-ga-name="gitlab milestones" data-ga-location="body"}.
              *   **Roadmaps:** Startdatum und/oder Fälligkeitsdatum können in Form einer Zeitleiste visualisiert werden. Die [Roadmap](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="roadmap" data-ga-location="body"}-Seite der Epics zeigt eine solche Visualisierung für alle Epics, die unter einer Gruppe und/oder ihren Untergruppen stehen.
              *   **Labels:** Erstelle und ordne einzelnen Tickets Labels zu. So kannst du die Ticketlisten nach einem einzelnen Label oder mehreren [Labels](https://docs.gitlab.com/ee/user/project/labels.html#prioritize-labels){data-ga-name="multiple labels" data-ga-location="body"} filtern.
              *   **Abarbeitungsdiagramm:** Verfolge die Arbeit in Echtzeit und entschärfe Risiken, sobald sie entstehen. [Abarbeitungsdiagramme](https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html#burndown-charts){data-ga-name="burndown" data-ga-location="body"} ermöglichen es Teams, die in einem aktuellen Sprint anfallenden Arbeiten zu visualisieren, während sie abgearbeitet werden.
              *   **Punkte und Schätzung:** Zeige den geschätzten Aufwand für Tickets an, indem du [gewichtete](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html){data-ga-name="weight" data-ga-location="body"} Attribute zuweist und den geschätzten Aufwand angibst
              *   **Zusammenarbeit:** Die Möglichkeit, auf Gesprächsbasis [zusammenzuarbeiten](https://docs.gitlab.com/ee/user/discussions/){data-ga-name="contribute" data-ga-location="body"}, wird überall in GitLab in Tickets, Epics, Merge Requests, Commits und mehr angeboten!
              *   **Rückverfolgbarkeit:** Verbinde die Tickets deines Teams mit nachfolgenden [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/){data-ga-name="merge requests" data-ga-location="body"}, was dir eine vollständige Rückverfolgbarkeit von der Erstellung des Tickets bis zum Ende der zugehörigen Pipeline ermöglicht.
              *   **Wikis:** Nutze ein System für die Dokumentation namens [Wiki](https://docs.gitlab.com/ee/user/project/wiki/){data-ga-name="wiki" data-ga-location="body"}, wenn du deine Dokumentation im selben Projekt pflegen willst, in dem sich auch dein Code befindet.
              *   **Enterprise Agile Frameworks:** Große Unternehmen haben Agile auf Unternehmensebene mit einer Vielzahl von Frameworks eingeführt. GitLab unterstützt [SAFe](https://www.scaledagileframework.com/), Spotify, Disciplined Agile Delivery und mehr.
    - name: 'copy-media'
      data:
        block:
          - header: Eine agile Iteration mit GitLab
            subtitle: User Storys → GitLab-Tickets
            text: |
              In Agile beginnst du oft mit einer User Story, die eine einzelne Funktion erfasst, die den Benutzer(inne)n einen geschäftlichen Nutzen bringt. In GitLab dient ein einzelnes Ticket innerhalb eines Projekts diesem Zweck.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-list.png
              alt: ""
          - subtitle: Aufgabe → GitLab-Aufgabenlisten
            inverted: true
            text: |
              Oft wird eine User Story weiter in einzelne Aufgaben unterteilt. Du kannst eine Aufgabenliste in der Beschreibung eines Tickets in GitLab erstellen, um die einzelnen Aufgaben zu identifizieren.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Tasks.png
              alt: ""
          - subtitle: Epics → GitLab Epics
            text: |
              In der anderen Richtung spezifizieren einige Agile-Anwender(innen) eine Abstraktion über den User Storys, die oft als Epic bezeichnet wird und einen größeren, aus mehreren Features bestehenden Benutzerfluss darstellt. In GitLab enthält ein Epic ebenfalls einen Titel und eine Beschreibung, ähnlich wie ein Ticket, aber es erlaubt dir, mehrere untergeordnete Tickets anzuhängen, um die Hierarchie zu verdeutlichen.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue.png
              alt: ""
          - subtitle: Produkt-Backlog → GitLab Ticketlisten und priorisierte Labels
            inverted: true
            text: |
              Die Eigentümer(innen) des Produkts oder des Unternehmens erstellen diese User Storys in der Regel, um die Bedürfnisse des Unternehmens und der Kund(inn)en zu berücksichtigen. Sie werden in einem Produkt-Backlog nach Prioritäten sortiert, um die Dringlichkeit und die gewünschte Reihenfolge der Entwicklung festzuhalten. Der/die Eigentümer(in) des Produkts kommuniziert mit den Stakeholdern, um die Prioritäten festzulegen, und verfeinert das Backlog ständig. In GitLab gibt es dynamisch erstellte Ticketlisten, die die Benutzer(innen) einsehen können, um ihr Backlog zu verfolgen. Es können Labels erstellt und einzelnen Tickets zugewiesen werden, so dass du die Ticketlisten nach einem oder mehreren Labels filtern kannst. Das sorgt für noch mehr Flexibilität. Mit Hilfe von Prioritätslabels kannst du die Tickets in diesen Listen auch sortieren.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-board.png
              alt: ""
          - subtitle: Sprints → GitLab-Meilensteine
            text: |
              Ein Sprint stellt einen begrenzten Zeitraum dar, in dem die Arbeit abgeschlossen werden soll, z. B. eine Woche, ein paar Wochen oder einen Monat oder mehr. Der/die Eigentümer(in) des Produkts und das Entwicklungsteam treffen sich, um zu entscheiden, welche Arbeiten für den kommenden Sprint vorgesehen sind. Die Meilensteinfunktion von GitLab unterstützt dies: Weise den Meilensteinen ein Start- und ein Fälligkeitsdatum zu, um den Zeitraum des Sprints zu erfassen. Das Team fügt dann Tickets in diesen Sprint ein, indem es sie dem jeweiligen Meilenstein zuweist.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Milestones.png
              alt: ""
          - subtitle: Punkte und Schätzung → GitLab Ticket-Gewichtungen
            inverted: true
            text: |
              In diesem Meeting werden auch die User Storys besprochen und für jede User Story im Sprint die technische Aufwandsstufe geschätzt. In GitLab haben Tickets ein Attribut „Gewichtung“, mit dem du den geschätzten Aufwand angeben kannst. In dieser Besprechung (oder in den folgenden) werden die User Storys weiter aufgeschlüsselt und manchmal werden technische Pläne und die Architektur dokumentiert. In GitLab können diese Informationen im Ticket oder in der Beschreibung des Merge Requests dokumentiert werden, da der Merge Request oft der Ort ist, an dem die technische Zusammenarbeit stattfindet. Während des Sprints (GitLab-Meilensteins) wählen die Mitglieder des Entwicklungsteams eine User Story nach der anderen aus, um sie zu bearbeiten. In GitLab haben Tickets Beauftragte. Du würdest dich also einem Ticket zuweisen, um zu zeigen, dass du jetzt daran arbeitest. Du solltest sofort einen leeren und mit einem Ticket verknüpften Merge Request erstellen, um die technische Zusammenarbeit zu beginnen, noch bevor du eine einzige Zeile Code geschrieben hast.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/weight.png
              alt: ""
          - subtitle: Agile Board → GitLab Ticketübersicht
            text: |
              Während des Sprints durchlaufen Tickets verschiedene Stadien, z. B. Ready for Dev, In Dev, In QA, In Review, Done, je nach dem Workflow in deiner Organisation. Normalerweise sind dies Spalten in einem Agile Board. In GitLab kannst du mit der Ticketübersicht deine Phasen definieren. Die Tickets durchlaufen diese dann nach und nach. Das Team kann die Übersicht in Bezug auf den Meilenstein und andere relevante Attribute konfigurieren. Während der täglichen Standups schaut das Team gemeinsam auf die Übersicht, um den Status des Sprints aus der Sicht des Workflows zu sehen.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue_board.png
              alt: ""
          - subtitle: Abarbeitungsdiagramme → GitLab Abarbeitungsdiagramme
            inverted: true
            text: |
              Das Entwicklungsteam möchte in Echtzeit wissen, ob es auf dem richtigen Weg ist, und Risiken abmildern, sobald sie auftreten. GitLab stellt Abarbeitungsdiagramme zur Verfügung, die es dem Team ermöglichen, die im aktuellen Sprint geplanten Arbeiten zu visualisieren, während sie abgearbeitet werden. Gegen Ende des Sprints stellt das Entwicklungsteam die fertiggestellten Funktionen den verschiedenen Interessengruppen vor. Mit GitLab wird dieser Prozess mithilfe von Review Apps vereinfacht, so dass auch Code, der noch nicht für die Produktion freigegeben ist, in verschiedenen Test-, Staging- oder UAT-Umgebungen vorgeführt werden kann. Die Funktionen Review Apps und CI/CD sind in den Merge Request selbst integriert. Die gleichen Tools sind auch für Entwickler(innen) und QA-Rollen nützlich, um die Softwarequalität zu sichern, sei es durch automatisierte Tests mit CI/CD oder durch manuelle Tests in einer Review App-Umgebung.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/burndown-chart.png
              alt: ""
    - name: copy-resources
      data:
        hide_horizontal_rule: true
        block:
          - subtitle: Bist du bereit für den nächsten Schritt?
            text: |
              Wenn du Fragen hast, sieh dir die [Hilfeseite von GitLab](/get-help/){data-ga-name="get help" data-ga-location="body"} an.
            resources:
              video:
                header: Videos
                links:
                  - text: Anleitung zum Einrichten von Agile Teams mit GitLab
                    link: https://youtu.be/VR2r1TJCDew
                  - text: Umsetze von SAFe (Scaled Agile Framework) mit GitLab
                    link: https://youtu.be/PmFFlTH2DQk
                  - text: Funktionsweise der Ticketübersicht von GitLab
                    link: https://youtu.be/CiolDtBIOA0
                  - text: Konfigurierbare Ticketübersicht
                    link: https://youtu.be/m5UTNCSqaDk
              blog:
                header: Blogs
                links:
                  - text: Anleitung für die Verwendung von GitLab für Agile
                    link: /blog/2018/03/05/gitlab-for-agile-software-development/
                    data_ga_name: GitLab for Agile
                    data_ga_location: body
                  - text: 4 Möglichkeiten zur Verwendung von Übersichten
                    link: /blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/
                    data_ga_name: 4 ways to use Boards
                    data_ga_location: body