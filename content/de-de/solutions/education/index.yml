---
  title: GitLab für den Bildungssektor
  description: Mithilfe von GitLab können Bildungseinrichtungen mit einer All-in-One-Plattform, auf der alle einen Beitrag leisten können, schnell modernisiert, innoviert und geschützt werden.
  components:
    - name: 'solutions-hero'
      data:
        note:
          - GitLab für den Bildungssektor
        title: Die Zukunft der Softwareentwicklung beginnt jetzt.
        subtitle: Biete allen Benutzer(innen) an deiner Schule hochentwickelte Enterprise-Funktionen.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimate kostenlos testen
          url: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/&glm_content=default-saas-trial

          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Vertrieb kontaktieren
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/education/education_hero.jpeg
          image_url_mobile: /nuxt-images/solutions/education/education_hero.jpeg
          alt: ""
          bordered: true
    - name: 'education-case-study-carousel'
      data:
        case_studies:
          - logo_url: /nuxt-images/logos/uw-logo.svg
            institution_name: University of Washington
            quote:
              img_url: /nuxt-images/solutions/education/university_of_washington.jpg
              quote_text: In den letzten zwei Jahren hat GitLab die Organisation hier an der UW transformiert. Die Plattform ist fantastisch!
              author: Aaron Timss
              author_title: Director of Information Technology, University of Washington
            case_study_url: /customers/uw/
            data_ga_name: University of Washington case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            institution_name: University of Surrey
            quote:
              img_url: /nuxt-images/solutions/education/university_of_surrey.jpg
              quote_text: An GitLab gefällt uns, dass die Plattform eine zentrale Lösung für viele Aufgaben bietet. Es handelt sich nicht nur um ein Repository&#58; Die Plattform hat viele Funktionen, einschließlich Wiki, Pages, Analytics, einer Web-Oberfläche, Merge-Anfragen, CI/CD und Kommentaren. Bei Problemen und Merge-Requests kann man zusammenarbeiten. Es ein schönes Gesamt-Paket.
              author: Kosta Polyzos
              author_title: Senior Systems Administrator, University of Surrey
            case_study_url: /customers/university-of-surrey
            data_ga_name: University of Surrey case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/victoria-university-wellington-logo.svg
            institution_name: Te Herenga Waka – Victoria Universität in Wellington
            quote:
              img_url: /nuxt-images/solutions/education/victoria.jpeg
              quote_text: Als ich von selbstverwaltetem GitLab hörte, war die Entscheidung klar. GitLab war wirklich das einzige Unternehmen, das die Anforderungen erfüllte, die ich im Rahmen der technischen Projektmanagementkurse hatte. Hinzu kommt, dass GitLab ein einziges Produkt ist.
              author: Dr. James Quilty
              author_title: Director of Engineering, Te Herenga Waka – Victoria University Wellington
            case_study_url: /customers/victoria_university
            data_ga_name: Victoria University of Wellington case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/deakin-university-logo.svg
            institution_name: Deakin University
            quote:
              img_url: /nuxt-images/solutions/education/deakin-case-study.jpg
              quote_text: Einer der Gründe für die Einführung von GitLab war die Anzahl an verschiedenen, sofort einsatzbereiten Sicherheitsfunktionen, die es uns ermöglichten, andere Lösungen und Open-Source-Tools und somit die damit verbundenen Fähigkeiten zu ersetzen.
              author: Aaron Whitehand
              author_title: Director of Digital Enablement, Deakin University
            case_study_url: /customers/deakin-university
            data_ga_name: Deakin University case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/heriot-watt-logo.svg
            institution_name: Heriot Watt University
            quote:
              img_url: /nuxt-images/solutions/education/heriot.jpeg
              quote_text: GitLab hat eine entscheidende Rolle bei der Vorbereitung der Studierenden auf die berufliche Praxis gespielt, insbesondere im Hinblick auf das Erlernen von Versionskontrolle, Builds und bewährten Methoden in der agilen Entwicklung.
              author: Rob Stewart
              author_title: Assistant Professor Computer Science, Heriot Watt Unversity
            case_study_url: /customers/heriot_watt_university
            data_ga_name: Heriot Watt University case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/dublin-city-university-logo.svg
            institution_name: Dublin City University
            quote:
              img_url: /nuxt-images/solutions/education/dublin-city-uni.jpg
              quote_text: "Es ist ein großer Pluspunkt für die DCU, dass sie GitLab als Teil des Lehrplans anbieten kann, um sicherzustellen, dass die Studierenden mit der Technologie auf dem neuesten Stand sind. Dadurch sind sie ausgezeichnet auf den Einstieg ins Berufsleben vorbereitet."
              author: Jacob Byrne
              author_title: Studierender im 4. Jahr, Dublin City University
            case_study_url: /customers/dublin-city-university
            data_ga_name: Dublin City University case study
            data_ga_location: case study carousel
    - name: 'case-study-tabs'
      data:
        heading: Warum solltest du dich für GitLab entscheiden?
        tabs:
          - tab: Schulen
            title: Unterstütze deine Bildungseinrichtung dabei, Software schneller und sicherer bereitzustellen – unabhängig davon, ob es sich um eine Hochschule, eine Universität, eine weiterführende Schule, eine Programmierschule oder eine andere Einrichtung handelt.
            text: Mit der DevSecOps-Plattform von GitLab hast du alles, was du brauchst, um Teams zusammenzubringen, um Zykluszeiten zu verkürzen, Kosten zu senken, die Sicherheit zu erhöhen und die Produktivität zu steigern.
            cta:
              text: Sprich mit einem Experten
              url: /sales/
              data_ga_name: talk to an expert
              data_ga_location: schools tab
            cards:
              - title: Beschleunige deine digitale Transformation
                icon_name: slp-digital-transformation
                text: |
                  Verwirkliche deine digitale Transformation mit einer einzigen Anwendung für den gesamten Softwareentwicklungsprozess. Die DevSecOps-Plattform von GitLab steht dir zur Seite:

                  - Modernisiere die Art und Weise, wie du Software erstellst und implementierst.
                  - Erhöhe die betriebliche Effizienz.
                  - Liefere schnell bessere Produkte.
                  - Senke das Sicherheits- und Konformitätsrisiko.

                cta:
                  text: Mehr erfahren
                  url: /solutions/digital-transformation/
                  data_ga_name: digital transformation
                  data_ga_location: schools tab
              - title: Verbessere die Zusammenarbeit mithilfe einer zentralen Plattform
                icon_name: slp-collaboration-alt-4
                text: |
                  Mit einer DevSecOps-Plattform, die für die Zusammenarbeit verteilter Teams entwickelt wurde, können alle Beteiligten – Entwickler, Betrieb, Sicherheit und sogar Lehrkräfte und Studierende – effizienter zusammenarbeiten. GitLab bietet:

                  - Projektplanung mit transparenten Tickets, Epics, Boards, Meilensteinen und mehr
                  - Robuste Versionskontrolle und -verlauf
                  - Merge-Requests zur Überprüfung und Diskussion von Code (und den Ergebnissen von Tests und Scans) vor dem Mergen
                cta:
                  text: Mehr erfahren
                  url: /platform/
                  data_ga_name: platform
                  data_ga_location: schools tab
              - title: Gewährleistung von Sicherheit und Konformität
                icon_name: slp-release
                text: |
                  Mit der DevSecOps-Plattform von GitLab kannst du deine Daten, Forschungen, Anwendungen, Kursunterlagen und alles andere sichern und gleichzeitig die Einhaltung von Vorschriften gewährleisten. Mit einem umfassenden Satz integrierter Sicherheitsscanner, Abhängigkeitsscans zur Unterstützung bei der Erstellung einer SBOM und konformen Pipelines zur Automatisierung von Sicherheits- und Konformitätsrichtlinien kannst du deine Anforderungen erfüllen, ohne an Geschwindigkeit einzubüßen.
                cta:
                  text: Mehr erfahren
                  url: /solutions/security-compliance/
                  data_ga_name: devsecops
                  data_ga_location: schools tab
          - tab: Lehrende
            title: Schließe dich Bildungseinrichtungen auf der ganzen Welt an und biete deinen Lernenden den entscheidenden Vorteil, indem du GitLab in den Unterricht und ins Forschungslabor bringst.
            text: GitLab ist eine einheitliche Plattform für Projektmanagement, Zusammenarbeit, Quellcodeverwaltung, Git, Automatisierung, Sicherheit und vieles mehr. Da sie einfach zu bedienen und flexibel ist und alles an einem Ort zusammenführt, ist sie die beste Wahl für die Integration von Branchenpraktiken auf dem Campus. Das GitLab for Education Programm stellt qualifizierten Institutionen auf der ganzen Welt kostenlose Lizenzen von GitLab für Lehre, Lernen und Forschung bereit!
            cta:
              text: Lass dich verifizieren
              url: /solutions/education/join
              data_ga_name: get verified
              data_ga_location: teachers tab
            cards:
              - title: GitLab im Unterricht
                icon_name: slp-user-laptop
                text: |
                  GitLab wird in Dutzenden von Disziplinen eingesetzt – nicht nur in der Programmierung. Mit der Integration von GitLab in den Unterricht erhalten Lernende nicht nur einen Vorteil durch das Erlernen einer branchenführenden Plattform, sondern sie lernen auch Projektmanagement, Zusammenarbeit, Versionskontrolle und betriebliche Workflows. GitLab kann als Single Source of Truth (SSOT) für End-to-End-Projekte von Lernenden in jeder Domäne verwendet werden.

                   Die DevSecOps-Funktionen von GitLab bieten Lernenden die Möglichkeit, domänenspezifische Fähigkeiten wie Softwareentwicklung, Infrastrukturmanagement, Informationstechnologie sowie Sicherheit zu erlernen. Testen von Code, kontinuierliche Integration, kontinuierliche Entwicklung und Sicherheitstests werden alle an einem Ort zusammengeführt. Gib deinen Lernenden das nötige Werkzeug an die Hand, mit dem sie bessere Lernergebnisse erzielen können.
                cta:
                  text: Lies die Ergebnisse der Umfrage „GitLab in Education“
                  url: /solutions/education/edu-survey
                  data_ga_name: gitlab education survey
                  data_ga_location: teachers tab
              - title: GitLab in der Forschung
                icon_name: slp-open-book
                text: |
                  GitLab transformiert den wissenschaftlichen Forschungsprozess über verschiedene Disziplinen hinweg, indem es das DevSecOps-Paradigma in die wissenschaftliche Gemeinschaft einbringt. Durch die Integration von GitLab in ihren Forschungsprozess erzielen Forscher(innen) mehr Transparenz, Zusammenarbeit, Reproduzierbarkeit, Geschwindigkeit der Ergebnisse und Datenintegrität. Mehrere Tausend von Experten begutachtete Publikationen wurden entweder über GitLab selbst oder mit in GitLab gespeicherten Daten veröffentlicht.
                cta:
                  text: Forschungsbeispiele ansehen
                  url: /blog/2022/02/15/devops-and-the-scientific-process-a-perfect-pairing/
                  data_ga_name: gitlab in research
                  data_ga_location: teachers tab
              - title: Lade uns auf deinen Campus ein
                icon_name: slp-institution
                text: |
                  Unser Team hilft dir oder deinen Lernenden gerne bei eurer DevSecOps-Entwicklung. Wir bieten Workshops sowie Gastvorträge und können mit deinen Lernenden über DevSecOps, GitLab und mehr sprechen.
                cta:
                  text: Besuch anfragen
                  url: https://docs.google.com/forms/d/e/1FAIpQLSeGya0P_GOn9QXR0VrQYoy7uCSJ2x6IryrKEGBzcmQmcDZv1g/viewform
                  data_ga_name: invite us to your campus
                  data_ga_location: teachers tab
          - tab: Lernende
            title: Verschaffe dir einen Vorsprung auf dem Arbeitsmarkt, indem du dir jetzt branchenführende Tools aneignest. Führende Unternehmen auf der ganzen Welt verwenden GitLab, um Software schneller und sicherer zu erstellen – und das kannst du auch.
            text: Beginne mit dem Aufbau eines Kompetenzportfolios über den gesamten DevSecOps-Lebenszyklus mit GitLab, während du noch in der Ausbildung bist. Fange klein an und lerne nach und nach dazu. Wir stehen dir zur Seite. 
            cta:
              text: Kundenstorys lesen
              url: /customers/
              data_ga_name: read case studies
              data_ga_location: students tab
            cards:
              - title: Tritt dem GitLab-Forum bei
                icon_name: slp-chat
                text: |
                  Komm vorbei und stell dich vor! Wir haben eine spezielle Kategorie „Bildung“ als Ressource für unsere Programmmitglieder erstellt. Betrachte das Forum als deine Ressource für Fragen und Antworten zu GitLab!
                cta:
                  text: Zum GitLab-Forum
                  url: https://forum.gitlab.com/c/gitlab-for-education/37?_gl=1*8fqeup*_ga*MTc1MDg3NzQ5Ni4xNjU1NDAwMTk3*_ga_ENFH3X7M5Y*MTY3MTczOTQyMC4xOTQuMS4xNjcxNzQyMzI4LjAuMC4w
                  data_ga_name: gitlab forum
                  data_ga_location: students tab
              - title: Veranstalte ein Meetup oder nimm daran teil
                icon_name: slp-calendar-alt-3
                text: |
                  Schau dir unsere GitLab-Meetups an! Sie sind eine großartige Möglichkeit, Expert(innen) in deiner Nähe zu treffen und mehr über DevSecOps zu erfahren. Du findest kein Meetup in deiner Nähe? Veranstalte ein Meetup in deiner Community. Es ist ganz einfach.
                cta:
                  text: Finde ein Meetup in deiner Nähe
                  url: /community/meetups
                  data_ga_name: meetups
                  data_ga_location: students tab
              - title: Nimm an einem Hackathon teil
                icon_name: slp-idea-collaboration
                text: |
                  Wir veranstalten vierteljährliche Hackathons, bei denen du einen Beitrag leisten, Preise gewinnen und dich mit anderen Mitwirkenden vernetzen kannst. Studierende sind besonders willkommen!
                cta:
                  text: Registriere dich für einen Hackathon
                  url: /community/hackathon/
                  data_ga_name: hackathon
                  data_ga_location: students tab
              - title: Erste Schritte
                icon_name: slp-pencil
                text: |
                  Bist du neu bei GitLab? Schaue dir unsere Lernressourcen an, um deine DevSecOps-Reise zu beginnen.
                cta:
                  text: Lernressourcen
                  url: https://docs.gitlab.com/ee/tutorials/
                  data_ga_name: getting started
                  data_ga_location: students tab
              - title: Werde Mitwirkende(r)
                icon_name: slp-code
                text: |
                  Du bist Studierende(r) und möchtest Erfahrungen sammeln? Möchtest du deine Fähigkeiten mithilfe eines Support-Netzwerks weiterentwickeln? Schaue dir unser Code-Beitragsprogramm an und lege los!
                cta:
                  text: Werde Mitwirkende(r)
                  url: /community/contribute
                  data_ga_name: contribute
                  data_ga_location: students tab
    - name: 'education-feature-tiers'
      data:
        heading: Bringe GitLab auf deinen Campus
        teacher_tier:
          title: Lehrende
          text: Kostenlose Bildungslizenzen zur Unterstützung von Lehre, Lernen und Forschung*
          features:
            - item: Unbegrenzte Plätze
            - item: Ultimate-Stufe
            - item: Beliebige Bereitstellungsmethode
            - item: 50.000 Recheneinheiten
            - item: Community-Support
            - item: Nur zu Lehr- und Forschungszwecken
          cta:
            text: Lass dich verifizieren
            url: /solutions/education/join/
            data_ga_name: get verified
            data_ga_location: education feature tiers
          disclaimer: '*Nicht zum Betrieb, zur Verwaltung oder zur Führung einer Institution zugelassen'
        schools_tier:
            title: Bildungseinrichtungen
            text: Zugriff auf die gesamte DevSecOps-Plattform für den gesamten Campus zu einem angepassten Preis
            features:
              - item: Unbegrenzte Plätze**
              - item: Ultimate-Stufe
              - item: Beliebige Bereitstellungsmethode
              - item: 50.000 Recheneinheiten
              - item: Priority-Support
              - item: Keine Anwendungsfallbeschränkungen
            cta:
              text: Vertrieb kontaktieren
              url: /sales/
              data_ga_name: contact sales
              data_ga_location: education feature tiers
            disclaimer: '**bis max. zur Immatrikulationszahl/Schülerzahl'
            callout_box:
              title: Oder ein Paket, das auf spezifische Bedürfnisse zugeschnitten ist
              features:
                - item: Genau die Anzahl der benötigten Plätze
                - item: Jede Stufe
                - item: Beliebige Bereitstellungsmethode
                - item: Recheneinheiten entsprechend der erworbenen Stufe
                - item: Priority-Support
                - item: Keine Anwendungsfallbeschränkungen
              cta:
                text: 20 % Rabatt erhalten
                url: /sales/
                data_ga_name: get discount
                data_ga_location: education feature tiers
    - name: 'education-stats'
      data:
        header: Werde Teil der GitLab-Community
        stats:
          - spotlight_text: &gt; 1.000
            text: Bildungseinrichtungen sind Teil des GitLab for Education-Programms.
          - spotlight_text: 3 Mio.
            text: Menschen (und steigend) nutzen GitLab in Bildungseinrichtungen.
          - spotlight_text: '65'
            text: Länder sind Teil des GitLab for Education-Programms.