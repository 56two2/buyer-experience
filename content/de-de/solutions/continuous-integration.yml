template: feature
title: 'Kontinuierliche Integration und Bereitstellung'
description: 'Wiederholbare und bedarfsgerechte Softwarebereitstellung'
components:
  feature-block-hero:
    data:
      title: Kontinuierliche Integration und Auslieferung
      subtitle: Wiederholbare und bedarfsgerechte Softwarebereitstellung
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        text: Kostenlose Testversion starten
        url: /free-trial/
      image:
        image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
        hide_in_mobile: true
        alt: "Bild: GitLab-Unendlichkeitsschleife"
  side-navigation:
    links:
      - title: Übersicht
        href: '#overview'
      - title: Vorteile
        href: '#benefits'
    data:
      feature-overview:
        data:
          content_summary_bolded: GitLab Kontinuierliche Integration und Bereitstellung
          content_summary: >-
            automatisiert alle Schritte, die zum Erstellen, Testen und Bereitstellen Ihres Codes in Ihrer Produktionsumgebung erforderlich sind. <br><br> <b>Kontinuierliche Integration</b> automatisiert die Builds, liefert Feedback über die Codeüberprüfung und automatisiert Codequalitäts- und Sicherheitstests. Es wird ein Release-Paket erstellt, das in Ihrer Produktionsumgebung bereitgestellt werden kann. <br><br> <b>Kontinuierliche Bereitstellung</b> stellt automatisch die Infrastruktur bereit und verwaltet Infrastrukturänderungen, Ticketing sowie die Versionsverwaltung. Sie ermöglicht die schrittweise Bereitstellung von Code, die Überprüfung und Überwachung der vorgenommenen Änderungen und die Möglichkeit, bei Bedarf ein Rollback vorzunehmen. Die Kombination aus kontinuierlicher Integration und Bereitstellung von GitLab hilft Ihnen dabei, Ihren Softwareentwicklungs-Lebenszyklus zu automatisieren und ihn mit minimalen manuellen Eingriffen wiederholbar und bedarfsgerecht zu gestalten.
          content_image: /nuxt-images/resources/resources_20.jpg
          content_heading: Ganze Workflows mit GitLab CI/CD automatisieren
          content_list:
            - Verbessern Sie die Produktivität Ihrer Entwickler(innen) durch kontextbezogene Testergebnisse.
            - Erstellen Sie sichere und konforme Releases, indem Sie jede Änderung testest.
            - Implementieren Sie Schutzmaßnahmen, um Ihre Bereitstellungen zu schützen.
            - Automatisieren Sie konsistente Pipelines für Einfachheit und Skalierbarkeit.
            - Automatisieren Sie sowohl Anwendungen als auch die Infrastruktur.
            - Beschleunigen Sie Ihren Wechsel zur Native Cloud und zur Hybrid Cloud.
      feature-benefit:
        data:
          feature_heading: Alles automatisieren
          feature_cards:
            - feature_name: Kontextabhängige Tests
              icon:
                name: automated-code
                variant: marketing
                alt: "Symbol: Automatisiertes Programmieren"
              feature_description: >-
                Jede Änderung löst eine automatisierte Pipeline aus, die Ihnen das Leben erleichtert.
              feature_list:
                - Testen Sie alles – Code, Performance, Auslastung, Sicherheit.
                - Ergebnisse werden mittels Merge Requests geteilt, bevor Entwickler(innen) sich einer neuen Aufgaben zuwenden.
                - Code wird kontextbezogen geprüft, diskutiert, verbessert und genehmigt.
            - feature_name: Sicher und konform
              icon:
                name: release
                variant: marketing
                alt: "Symbol: Schutzschild mit Häkchen"
              feature_description: >-
                Von Konformitätspipelines bis hin zu integriertem Sicherheitsscanning – Sie finden alles an einem Ort und haben so eine bessere Übersicht und Kontrolle.
              feature_list:
                - Anwendungssicherheitstests sind in den Workflow der Entwickler(innen) integriert.
                - Schwachstellenmanagement für Sicherheitsprofis.
                - Genehmigungen, Prüfberichte und Rückverfolgbarkeit zur Vereinfachung der Richtlinienkonformität.
            - feature_name: Implementieren Sie Schutzmaßnahmen, um Ihre Bereitstellungen zu schützen.
              icon:
                name: approve-dismiss
                variant: marketing
                alt: "Symbol: Zustimmen/Ablehnen"
              feature_description: >-
                Mit Strategien zum Schutz Ihrer Bereitstellungen verhindern Sie, dass die Bereitstellung fehlschlägt oder nicht funktioniert.
              feature_list:
                - Konformitätspipelines stellen sicher, dass die Richtlinien konsequent befolgt werden.
                - Feature-Flags bieten granulare Kontrolle über die Bereitstellung.
                - Mithilfe von progressiver, Canary- und Blau-Grün-Bereitstellungen entscheiden Sie, für wen Sie bereitstellen möchten.
            - feature_name: Pipelines, die auf Einfachheit und Skalierbarkeit ausgelegt sind
              icon:
                name: pipeline-alt
                variant: marketing
                alt: "Symbol: Pipeline"
              feature_description: >-
                Flexible Pipelines unterstützen Sie in jeder Phase Ihrer Reise.
              feature_list:
                - Integrierte Vorlagen erleichtern den Einstieg.
                - Pipelines können automatisch über Auto DevOps erstellt werden.
                - Skalierung mit übergeordneten Pipelines und Merge Trains.
            - feature_name: Automatisieren von Anwendungen und Infrastruktur
              icon:
                name: devsecops
                variant: marketing
                alt: "Symbol: DevSecOps-Kreis"
              feature_description: >-
                Eine einzige Plattform zur Automatisierung von Anwendung und Infrastruktur.
              feature_list:
                - Minimieren Sie Lizenzkosten und die Lernkurve.
                - Nutzen Sie die bewährten Methoden von Application DevSecOps für die Infrastruktur.
            - feature_name: Beschleunigen Sie Ihre Reise in die Native Cloud und die Hybrid Cloud.
              icon:
                name: continuous-integration
                variant: marketing
                alt: "Symbol: kontinuierliche Integration"
              feature_description: >-
                GitLab ist ein infrastrukturunabhängiges DevSecOps, das für die Multicloud entwickelt wurde.
              feature_list:
                - Unterstützt die Bereitstellung auf virtuellen Maschinen, Kubernetes-Clustern oder FaaS von verschiedenen Cloud-Anbietern.
                - Unterstützt Amazon Web Services, Google Cloud Platform, Microsoft Azure oder Ihre eigene private Cloud.
  group-buttons:
    data:
      header:
        text: Erkunden Sie weitere Möglichkeiten, wie GitLab Sie bei der kontinuierlichen Integration unterstützen kann.
        link:
          text: Weitere Lösungen
          href: /solutions/
      buttons:
        - text: Automatische Bereitstellung
          icon_left: automated-code
          href: /solutions/delivery-automation/
        - text: Kontinuierliche Softwaresicherheit
          icon_left: devsecops
          href: /solutions/continuous-software-security-assurance/
        - text: Konformität
          icon_left: shield-check
          href: /solutions/compliance/
  report-cta:
    layout: "dark"
    title: Analystenberichte
    reports:
    - description: "GitLab als einziger Marktführer in The Forrester Wave™ anerkannt: Integrierte Software-Entwicklungsplattformen, Q2 2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      link_text: Lesen Sie den Bericht
    - description: "GitLab als Marktführer im Gartner® Magic Quadrant 2023™ für DevOps-Plattformen anerkannt"
      url: /gartner-magic-quadrant/
      link_text: Lesen Sie den Bericht
  solutions-resource-cards:
    data:
      title: Ressourcen
      link:
        text: Alle Ressourcen anzeigen
      column_size: 4
      cards:
        - icon:
            name: ebook
            variant: marketing
            alt: "Symbol: E-Book"
          event_type: E-Book
          header: Wie man die Unternehmensführung davon überzeugt, CI/CD einzuführen
          link_text: Mehr lesen
          image: "/nuxt-images/resources/resources_1.jpeg"
          href: https://page.gitlab.com/2021_eBook_leadershipCICD.html
          data_ga_name: How to convince your leadership to adopt CI/CD
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: "Symbol: Fallstudie"
          event_type: Fallstudie
          header: >-
            Fünf AWS-Kunden, die sich bei ihrem DevSecOps-Workflow auf GitLab verlassen
          link_text: Mehr lesen
          href: https://about.gitlab.com/resources/ebook-five-aws-customers/
          image: /nuxt-images/features/resources/resources_case_study.png
          data_ga_name: Five AWS customers who depend on GitLab for DevSecOps workflow
          data_ga_location: body
        - icon:
            name: webcast
            variant: marketing
            alt: "Symbol: Webcast"
          event_type: Webcast
          header: 7 GitLab CI/CD-Hacks
          link_text: Jetzt ansehen
          href: https://about.gitlab.com/webcast/7cicd-hacks/
          image: /nuxt-images/features/resources/resources_webcast.png
          data_ga_name: 7 GitLab CI/CD hacks
          data_ga_location: body
        - icon:
            name: video
            variant: marketing
            alt: "Symbol: Video"
          event_type: Video
          header: Erste Schritte mit GitLab CI/CD
          link_text: Jetzt ansehen
          href: https://www.youtube.com/embed/sIegJaLy2ug
          image: /nuxt-images/features/resources/resources_webcast.png
          data_ga_name: Getting started with GitLab CI/CD
          data_ga_location: body
        - icon:
            name: video
            variant: marketing
            alt: "Symbol: Video"
          event_type: Video
          header: Übersicht über CI/CD
          link_text: Jetzt ansehen
          href: https://www.youtube.com/embed/l5705U8s_nQ?start=397
          image: /nuxt-images/resources/fallback/img-fallback-cards-cicd.png
          data_ga_name: CI/CD Overview
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: "Symbol: Fallstudie"
          event_type: Fallstudie
          header: Automatische Skalierung für schnellere Pipelines
          link_text: Mehr lesen
          href: https://about.gitlab.com/customers/EAB/
          image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
          data_ga_name: Auto-scaling to achieve faster pipelines
          data_ga_location: body