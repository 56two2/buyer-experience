---
  data:
    competitor: Snyk
    gitlab_coverage: 50
    competitor_coverage: 50
    subheading: Finds and automatically fixes vulnerabilities in code, open source dependencies, containers, and infrastructure as code.
    comparison_table:
      - stage: Secure
        features:
          - feature: SAST
            gitlab:
              coverage: 100
              projected_coverage: 100
              description: Our comprehensive Static Application Security Testing (SAST) solution covers the essentials for checking your source code for known vulnerabilities. The product experience team found more security issues than our competition (in a POC), while supporting a greater number of languages than many of our competitors. 
              details: |
                * Static Application Security Testing scans the application source code and binaries to spot potential vulnerabilities before deployment using open source tools that are installed as part of GitLab.
                * Vulnerabilities are shown in-line with every merge request and results are collected and presented as a single report.
              improving_product_capabilities: |
                * [Support for more Languages](https://gitlab.com/groups/gitlab-org/-/epics/297){data-ga-name="link to support for more languages" data-ga-location="body"}.
                * Implement next generation [generic language-agnostic scanning](https://gitlab.com/groups/gitlab-org/-/epics/3260){data-ga-name="link to generic language-agnostic scanning" data-ga-location="body"} approach.
                  * Improvements to vulnerability detection engine, [vulnerability fingerprinting and tracking accuracy](https://gitlab.com/groups/gitlab-org/-/epics/5144){data-ga-name="link to vulnerability fingerprinting and tracking accuracy" data-ga-location="body"}, as well as help reduce false positives to [provide developers increased context for taking action](https://gitlab.com/gitlab-org/gitlab/-/issues/284337){data-ga-name="link to provide developers increased context for taking action" data-ga-location="body"} to remediate SAST findings.
                * Proprietary SAST tool built upon research by our [Vulnerability Research Team](https://about.gitlab.com/handbook/engineering/development/sec/secure/vulnerability-research/){data-ga-name="link to vulnerability research team" data-ga-location="body"}.
                * Use advanced detection techniques like [abstract syntax tree parsing](https://en.wikipedia.org/wiki/Abstract_syntax_tree){data-ga-name="link to abstract syntax tree parsing" data-ga-location="body"}.
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/sast/
            competitor:
              coverage: 100
              description: Snyk’s solution covers all the basics for SAST, however their product experience found fewer security issues despite marketing their innovations on AI based code scanning.
              details: |
                * Snyk Code is based on a deep-code, semantic-code Analysis Engine, which uses AI to continuously learn from billions of lines of code, and 100s of millions of code fixes, in the global development community. The Snyk Code AI Engine continuously evolves the human-guided reinforced learning cycle lead by Snyk's security researchers and engineers.
                * Supports [fewer languages](https://docs.snyk.io/products/snyk-code/snyk-code-language-and-framework-support#language-support-with-snyk-code-ai-engine){data-ga-name="link to fewer languages" data-ga-location="body"} than GitLab.
              link_to_documentation: https://docs.snyk.io/products/snyk-code
          - feature: Secret Detection
            gitlab:
              coverage: 50
              projected_coverage: 75
              description: GitLab's Secret Detection is designed to keep your sensitive data secure. By proactively identifying and addressing potential security vulnerabilities with a customizable solution and a default ruleset of more than 90 secret detection patterns, GitLab's Secret Detection can help you maintain the highest standards of data protection and keep your organization safe from data breaches. 
              details: |
                * Secret Detection uses a default ruleset containing more than 90 secret detection patterns. Users can also customize the secret detection patterns using custom rulesets. If users want to contribute rulesets for “well-identifiable” secrets, follow the steps detailed in the community contributions guidelines.
                * Scanning can be done either for the entire Git history or for just the current code.
              improving_product_capabilities: |
                * [Allow git history to be scrubbed](https://gitlab.com/gitlab-org/gitlab/-/issues/212763){data-ga-name="link to allow git history to be scrubbed" data-ga-location="body"}.
                * [Auto Remediation for Secret Detection](https://gitlab.com/groups/gitlab-org/-/epics/2451){data-ga-name="link to auto remediation for secret detection" data-ga-location="body"}.
                * Expanding the set of providers that support [post-processing and revocation](https://docs.gitlab.com/ee/user/application_security/secret_detection/post_processing.html){data-ga-name="link to post-processing and revocation" data-ga-location="body"}.
                * Expanding the quality and completeness of secret detection patterns.
                * Improving the usability of secret detection, for instance by making it possible to run historical secret scanning as an on-demand job.
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/secret_detection/
            competitor:
              coverage: 75
              description: Snyk’s solution covers all the basics for Secret Detection, however they are marketing their innovations on AI based code scanning.
              details: |
                * Snyk Code includes secret detection capabilities that scan and highlight secrets like keys, credentials, PII, and sensitive information in source code. Unlike tools that use entropy checks or regular expressions, Snyk Code uses machine learning to improve the accuracy of detecting secrets and minimizing the occurrence of false positives.
                * No information regarding how this works; rating is provided strictly by features.
              link_to_documentation: https://docs.snyk.io/products/snyk-code/introducing-snyk-code/key-features/ai-engine#hardcoded-secrets
          - feature: Code Quality
            gitlab:
              coverage: 50
              projected_coverage: 100
              description: GitLab’s code quality scanner is built to ensure your code stays clean, reliable and consistent.This helps keep your project’s code simple, readable, and easier to maintain, while verifying changes don’t degrade your code’s quality, before committing them to the default branch. While still evolving as a capability, it can be extended, and has a significant number of features planned on our roadmap.
              details: |
                * To ensure a project’s code stays simple, readable, and easy to contribute to, users can use GitLab CI/CD to analyze their source code quality.
                * Uses plugins supported by Code Climate, which are free and open source. Code Quality does not require a Code Climate subscription.
                * Can make use of a template.
                * Can be extended through Analysis Plugins or a custom tool.
                * In-code results.
              improving_product_capabilities: |
                * [Completing support](https://gitlab.com/gitlab-org/gitlab/-/issues/328257){data-ga-name="link to completing support" data-ga-location="body"} for multiple quality reports in merge request diffs.
                  * Rollout for this feature is currently blocked by performance concerns, which are a significant issue to address for Code Quality overall. [Our plan for upcoming milestones](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=group%3A%3Astatic%20analysis&label_name%5B%5D=Planning%20Issue&first_page_size=20){data-ga-name="link to plan for upcoming milestones" data-ga-location="body"} includes [diagnosing these performance issues](https://gitlab.com/gitlab-org/gitlab/-/issues/358759){data-ga-name="link to diagnosing these performance issues" data-ga-location="body"}.
                * Evolving the design for [inline findings](https://docs.gitlab.com/ee/ci/testing/code_quality.html#code-quality-in-diff-view){data-ga-name="link to inline findings" data-ga-location="body"} toward [newer designs that account for security findings](https://gitlab.com/gitlab-org/gitlab/-/issues/322689){data-ga-name="link to newer designs that account for security findings" data-ga-location="body"}.
                * [Merge Request approval rules](https://gitlab.com/gitlab-org/gitlab/-/issues/34982){data-ga-name="link to merge request approval rules" data-ga-location="body"} based on quality findings, similar to security policies.
                * [Result filtering](https://gitlab.com/gitlab-org/gitlab/-/issues/238858){data-ga-name="link to result filtering" data-ga-location="body"} to allow teams to set a threshold for the minimum severity level of issues they wish to see.
              link_to_documentation: https://docs.gitlab.com/ee/ci/testing/code_quality.html
            competitor:
              coverage: 75
              description: Snyk’s solution covers all the basics for Code Quality, however their product experience found fewer code quality issues despite marketing their innovations on AI based code scanning.
              details: |
                * Problems such as dead code, branches that are predefined, and branches having the same code on each side.
                * Determining the initial type and its changes--this is of special interest for dynamically typed languages.
                * Infers possible values for variables used to call functions to track off-by-one errors in arrays, division-by-zero, and null dereferences.
                * Follows the flow of data within the application from the source to the sink. Combined with AI-based learning of external insecure data source, data sinks, and sanitation functions, this enables a strong taint analysis.
                * Identifies multiple potential issues including API misuses, null dereferences, and type mismatches by modeling the usage of memory in variables and references. This mechanism can also identify use of insecure functions.
                * Identifies null dereference or race conditions by modeling each possible control flow in the application.
                * Identifies multiple potential issues including buffer overruns, null dereferences, and type mismatches by modeling the usage of memory in variables and references.
                * Code quality findings are available in GitLab, but not using Snyk in the same repo
              link_to_documentation: https://docs.snyk.io/products/snyk-code/pr-checks-for-snyk-code
          - feature: DAST
            gitlab:
              coverage: 100
              projected_coverage: 100
              description: Deploying your web application into a new environment can lead to your application becoming exposed to new types of attacks. GitLab’s Dynamic Application Security Testing (DAST) is an extremely mature solution for scanning running applications via Web UI. It examines applications for vulnerabilities like misconfigurations of application servers or incorrect assumptions about security controls in deployed environments, which are often not visible in the source code.
              details: |
                * Dynamic Application Security Testing (DAST) examines applications for vulnerabilities in deployed environments.
                * DAST uses the open source tool OWASP Zed Attack Proxy for analysis.
                * Can be configured with Active(perform an active scan to attack an application and produce a more extensive security report) and Passive (doesn’t actively attack the application) mode.
                * Can be used with review apps.
                * Support for Authentication.
                * Can be run On Demand, via pipeline, or on schedule.
              improving_product_capabilities: |
                * [On-demand DAST improvements](https://gitlab.com/groups/gitlab-org/-/epics/6778){data-ga-name="link to on-demand dast improvements" data-ga-location="body"}.
                * [DAST Pre-flight validation](https://gitlab.com/groups/gitlab-org/-/epics/7069){data-ga-name="link to dast pre-flight validation" data-ga-location="body"}.
                * [Browser-based scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4248){data-ga-name="link to browser-based scanner for dast" data-ga-location="body"}.
                  * [Crawlgraph SVG artifact for browser-based scans](https://gitlab.com/gitlab-org/gitlab/-/issues/345354){data-ga-name="link to crawlgraph svg artifact for browser-based scans" data-ga-location="body"}.
                  * [Browser-based passive vulnerability checks](https://gitlab.com/groups/gitlab-org/-/epics/5779){data-ga-name="link to browser-based passive vulnerability checks" data-ga-location="body"}.
                  * [Browser-based active vulnerability checks](https://gitlab.com/groups/gitlab-org/-/epics/5780){data-ga-name="link to browser-based active vulnerability checks" data-ga-location="body"}.
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/secret_detection/
            competitor:
              coverage: 0
          - feature: API Security
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: GitLab offers API Security as a solution for scanning running applications via their API, incorporating everything from API discovery and management to DAST API and API fuzz testing to active API defense. By integrating API security into pipelines and leveraging review apps to dynamically test applications, developers can understand the security impact of their code on the running application, fixing issues before merging features.
              details: |
                * Users can add dynamic application security testing (DAST) of web APIs to their GitLab CI/CD pipelines. This helps to discover bugs and potential security issues that other QA processes may miss.
                * Users can run DAST API scanning against the following web API types:
                  * REST API.
                  * SOAP.
                  * GraphQL.
                  * Form bodies, JSON, or XML.
                * Web API fuzzing performs fuzz testing of API operation parameters. Fuzz testing sets operation parameters to unexpected values in an effort to cause unexpected behavior and errors in the API backend.
              improving_product_capabilities: |
                * [API Security scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254){data-ga-name="link to api security scanner for dast" data-ga-location="body"}.
                * [API Security scanner speed improvements](https://gitlab.com/groups/gitlab-org/-/epics/6502){data-ga-name="link to api security scanner speed improvements" data-ga-location="body"}.
                * [API Discovery](https://gitlab.com/groups/gitlab-org/-/epics/7539){data-ga-name="link to api discovery" data-ga-location="body"}.
                * [Java Spring Boot Rest API Discovery](https://gitlab.com/gitlab-org/gitlab/-/issues/362659){data-ga-name="link to java spring boot rest api discovery" data-ga-location="body"}.
                * [API Security gRPC support](https://gitlab.com/gitlab-org/gitlab/-/issues/244492){data-ga-name="link to api security grpc support" data-ga-location="body"}.
                * [API Security Parameter exclusion](https://gitlab.com/gitlab-org/gitlab/-/issues/292196){data-ga-name="link to api security parameter exclusion" data-ga-location="body"}.
                * [API Security support for dotenv based dynamic environments](https://gitlab.com/gitlab-org/gitlab/-/issues/247641){data-ga-name="link to api security support for dotenv based dynamic environments" data-ga-location="body"}.
                * [API Security x-request-id header](https://gitlab.com/gitlab-org/gitlab/-/issues/329722){data-ga-name="link to api security x-request-id header" data-ga-location="body"}.
                * [API Security sample data generation for XML](https://gitlab.com/gitlab-org/gitlab/-/issues/320842){data-ga-name="link to api security sample data generation for xml" data-ga-location="body"}.
                * [API Security Authentication improvements](https://gitlab.com/groups/gitlab-org/-/epics/3932){data-ga-name="link to api security authentication improvements" data-ga-location="body"}.
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/dast_api/
            competitor:
              coverage: 0
          - feature: Fuzz Testing
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: GitLab offers both traditional and coverage-guided fuzzing to find bugs and errors within source code. This helps you discover the bugs and potential security issues that other QA processes may miss. The key advantages of fuzz testing are that it can find not only security issues, but also flaws in the business logic of an application or service. Having fuzz testing is another key security component of a comprehensive DevSecOps platform.
              details: |
                * Coverage-guided fuzz testing sends random inputs to an instrumented version of an application in an effort to cause unexpected behavior. Such behavior indicates a bug that should be addressed.
                * Traditional fuzz testing uses a definition of the inputs a target application is expecting, to better understand the Implementation Under Test (IUT). Traditional fuzz testing is able to observe how the behavior of the IUT changes to make different decisions for subsequent fuzz tests. This approach provides better quality of results by inducing more faults and helping to pinpoint where they are in the app.
              improving_product_capabilities: |
                * [Support Continuous Fuzzing for Coverage Guided Fuzzing](https://gitlab.com/groups/gitlab-org/-/epics/4486){data-ga-name="link to support continuous fuzzing for coverage guided fuzzing" data-ga-location="body"}.
                * [Coverage-guided fuzz testing language support (Complete maturity)](https://gitlab.com/groups/gitlab-org/-/epics/5396){data-ga-name="link to coverage-guided fuzz testing language support" data-ga-location="body"}.
                * [Protocol fuzz testing integration into GitLab](https://gitlab.com/groups/gitlab-org/-/epics/5116){data-ga-name="link to protocol fuzz testing integration into gitlab" data-ga-location="body"}.
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
            competitor:
              coverage: 0
          - feature: Dependency Scanning
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: The Dependency Scanning feature can automatically find security vulnerabilities in software dependencies while developing and testing applications. For example, dependency scanning lets you know if your application uses an external (open source) library that is known to be vulnerable. With GitLab CI/CD, you can use dependency scanning to analyze your dependencies for known vulnerabilities. GitLab scans all dependencies, including transitive dependencies (also known as nested dependencies).
              details: |
                * Dependency Scanning analyzes a user’s project and tells them which software dependencies, including upstream dependencies, have been included in their project, and what known risks the dependencies contain.
                * Dependency Scanning modifies its behavior based on the language and package manager of the project to increase the accuracy of results.
              improving_product_capabilities: |
                * [Continuous vulnerability scans](https://gitlab.com/groups/gitlab-org/-/epics/7886){data-ga-name="link to continuous vulnerability scans" data-ga-location="body"}:
                  * Alert when the advisory database is updated and new vulnerabilities are detected in previously scanned code (Epic/Issue creation in progress).
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
            competitor:
              coverage: 75
              description: Snyk Dependency Scanning is comparable to GitLab, but additionally allows for easy one-click remediation, which automates the PR creating process and works on multiple languages.
              details: |
                * After Git repositories are imported to Snyk, Snyk continuously monitors these repositories by regularly scanning them for vulnerability, license, and dependency health issues.
                * In addition to providing fix advice, Snyk can also automatically create pull requests (PRs) on a user’s behalf, in order to upgrade their dependencies based on the scan results.
                * Automatic dependency upgrade pull requests feature for npm, Yarn, and Maven-Central repositories
          - feature: Container Scanning
            gitlab:
              coverage: 50
              projected_coverage: 100
              description: Container Scanning is a crucial element of container security. GitLab’s Container Scanning solution can be used to audit your Docker-based apps. GitLab compares the found vulnerabilities between the source and target branches, and shows the information directly in the merge request.
              details: |
                * An application’s Docker image may itself be based on Docker images that contain known vulnerabilities. By including an extra Container Scanning job in a pipeline that scans for those vulnerabilities and displays them in a merge request, users can use GitLab to audit their Docker-based apps.
                * Can scan images in a remote registry (but done one by one).
                * [Operational Container Scanning](https://docs.gitlab.com/ee/user/clusters/agent/vulnerabilities.html){data-ga-name="link to operational container scanning" data-ga-location="body"} can find vulnerabilities in our cluster’s container images.
              improving_product_capabilities: |
                * Increase usability:
                  * [Continuous vulnerability scans](https://gitlab.com/groups/gitlab-org/-/epics/7886){data-ga-name="link to continuous vulnerability scans" data-ga-location="body"}.
                  * [Better support scanning of multiple images](https://gitlab.com/groups/gitlab-org/-/epics/3139){data-ga-name="link to better support scanning of multiple images" data-ga-location="body"}.
                  * [Simplify setup for AWS ECR images](https://gitlab.com/groups/gitlab-org/-/epics/6145){data-ga-name="link to simplify setup for aws ecr images" data-ga-location="body"}.
                * Decrease noise:
                  * [Group/consolidate similar findings](https://gitlab.com/groups/gitlab-org/-/epics/5801){data-ga-name="link to group/consolidate similar findings" data-ga-location="body"}.
                  * [Prioritize findings that are fixable by the dev team](https://gitlab.com/groups/gitlab-org/-/epics/6846){data-ga-name="link to prioritize findings that are fixable by the dev team" data-ga-location="body"}.
                  * [Identify false positives](https://gitlab.com/gitlab-org/gitlab/-/issues/10046){data-ga-name="link to identify false positives" data-ga-location="body"}.
                * Integrate with the rest of GitLab:
                  * [Automatically scan GitLab's container registry](https://gitlab.com/groups/gitlab-org/-/epics/2340){data-ga-name="link to automatically scan gitlab's container registry" data-ga-location="body"}.
                  * Alert when the database is updated and vulnerabilities exist in previously-scanned images (Epic/Issue creation in progress).
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/container_scanning/
            competitor:
              coverage: 75
              description: Container scanning solution is acceptable and configurable, but has nothing unique other than making it easier to scan a container registry.
              details: |
                * Provides tools and integrations for quickly finding and fixing these vulnerabilities.
                * Allows users to create images with security built-in from the start.
                * Provides History of when vulns were detected.
                * Easier to scan many images in container registry.
                * Can scan a running workload with Kubernetes Integration.
              link_to_documentation: https://docs.snyk.io/products/snyk-container
          - feature: IaC Scanning
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: Infrastructure as Code (IaC) Scanning scans your IaC configuration files for known vulnerabilities. GitLab offers IaC scanning for all the top types of IaC configurations, and vulnerabilities can be managed and accessed through the security dashboard.
              details: |
                * Currently, IaC scanning supports configuration files for Terraform, Ansible, AWS CloudFormation, and Kubernetes.
                * Users can manage vulnerabilities using the vulnerability page
                * Users have access to the security dashboard.
              improving_product_capabilities: 
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/iac_scanning/
            competitor:
              coverage: 75
              description: Snyk offers a similar solution to GitLab, however adds additional support with custom rules for detecting and categorizing IaC vulnerabilities.
              details: |
                * The Snyk CLI for Infrastructure as Code provides immediate local feedback as configurations are written, so issues can be fixed before they are committed.
                * Integrate Snyk into the CI/CD processes to automate security checks.
                * Import source repositories into Snyk for ongoing monitoring and analysis.
                * Integrate with Hashicorp Terraform Cloud to scan as part of a deployment pipeline.
                * Snyk IaC has a comprehensive set of predefined security rules, based on industry benchmarks, cloud-provider best practices, and threat model research from Snyk’s security intelligence team.
                * Users can also build custom rules, leveraging Open Policy Agent (OPA).
              link_to_documentation: https://docs.snyk.io/products/snyk-infrastructure-as-code
        overview_analysis: The Secure stage consists of many different features and has been a key driver for Enterprise adoption of Ultimate. While some of the more basic security elements have little disparity, Snyk differentiates with the ability to configure rules for scanning IaC and ML algorithms.
        gitlab_product_roadmap:
          - roadmap_item: Move Dynamic Analysis (DAST), API Security, Dependency Scanning, and Vulnerability Management categories to Complete maturity.
          - roadmap_item: Returning License Compliance to Viable maturity.
    competitor_cards:
      title: "More comparisons"
      cards:
        - name: "Harness"
          icon: release
          stage: Release
          description: How does GitLab compare to Harness in the Release stage?
          link: /competition/harness/
          data_ga_name: link to gitlab vs harness
          data_ga_location: body
        - name: "Datadog"
          icon: monitor-alt-2
          stage: Monitor
          description: How does GitLab compare to Datadog in the Monitor stage?
          link: /competition/datadog/
          data_ga_name: link to gitlab vs datadog
          data_ga_location: body
        - name: "Atlassian"
          icon: plan
          stage: Plan
          description: How does GitLab compare to Atlassian in the Plan stage?
          link: /competition/atlassian/
          data_ga_name: link to gitlab vs atlassian
          data_ga_location: body
