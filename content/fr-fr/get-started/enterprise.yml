---
title: Premier pas avec GitLab - guide pour les entreprises
description: 'Ce guide vous aidera à configurer rapidement les éléments essentiels au développement et la livraison automatisés de logiciels en  utilisant l''édition Premium'
side_menu:
  anchors:
    text: Chapitres
    data:
      - text: Premiers pas
        href: '#getting-started'
        data_ga_name: getting-started
        data_ga_location: side-navigation
      - text: Installation
        href: '#getting-setup'
        data_ga_name: getting-setup
        data_ga_location: side-navigation
      - text: Utiliser GitLab
        href: '#using-gitlab'
        data_ga_name: using-gitlab
        data_ga_location: side-navigation
  hyperlinks:
    text: ''
    data: []
hero:
  crumbs:
    - title: Premiers pas
      href: /get-started/
      data_ga_name: Get Started
      data_ga_location: breadcrumb
    - title: Premier pas avec GitLab - guide pour les entreprises
  header_label: 'Durée : 20 min'
  title: 'Premier pas avec GitLab - guide pour les entreprises'
  content: |
    Pour rester compétitif, vous avez besoin d'une solution pour simplifier et mettre à l'échelle votre approche DevSecOps afin que vos équipes puissent livrer du code sécurisé plus rapidement. Ce guide vous aidera à configurer rapidement les éléments essentiels au développement et la livraison automatisés de logiciels en utilisant l'édition Premium, avec des options incluant la sécurité, la conformité et la planification de projets incluses dans l'édition Ultimate.
steps:
  text:
    show: Afficher tout
    hide: Masquer tout
  groups:
    - header: Pour commencer
      id: 'getting-started'
      items:
        - title: "Déterminer l'abonnement qui vous convient"
          copies:
            - title: 'GitLab SaaS ou GitLab autogéré'
              text: |
                <p>Voulez-vous que GitLab gère votre plateforme GitLab ou souhaitez-vous la gérer vous-même ?</p>
              link:
                href: https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed
                text: 'Voir les différences'
                ga_name: GitLab Saas vs Self-Managed
                ga_location: body
        - title: 'Déterminer le niveau qui répondra à vos besoins'
          copies:
            - title: 'Premium ou Ultimate'
              text: |
                <p>Pour déterminer le niveau qui vous convient, tenez compte des éléments suivants : </p>
            - title: Quantité de stockage nécessaire
              text: |
                <p>Les espaces de noms de niveau Premium sur GitLab SaaS ont une limite de stockage de 50 Go et les espaces de noms de niveau Ultimate vont jusqu'à 250 Go.</p>
            - title: Sécurité et conformité souhaitées
              text: |
                <p>* La détection des secrets, SAST et l'analyse des conteneurs sont disponibles dans la version Premium.</p>

                <p>* Des scanners supplémentaires <a href="https://docs.gitlab.com/ee/user/application_security/">tels que DAST, dépendances, images Cluster, IaC, APIs,</a> et fuzzing sont disponibles dans Ultimate.</p>

                <p>* Les résultats exploitables, intégrés dans le pipeline des demandes de fusion et dans le tableau de bord de la sécurité, nécessitent une gestion optimale des vulnérabilités.</p>

                <p>* Les pipelines de conformité requièrent de l'Ultimate.</p>

                <p>* Découvrez nos scanners de sécurité <a href="https://docs.gitlab.com/ee/user/application_security/"> et nos capacités</a> de <a href="https://docs.gitlab.com/ee/administration/compliance.html">mise en conformité </a>.
              link:
                href: /pricing/
                text: 'Consultez nos tarifs pour en savoir plus'
                ga_name: pricing
                ga_location: body
    - header: "S'installer"
      id: 'getting-setup'
      items:
        - title: "Créez votre compte d'abonnement SaaS"
          copies:
            - title: 'Déterminez le nombre de sièges que vous souhaitez'
              text: |
                "Un abonnement GitLab SaaS utilise un modèle concurrent (siège). Vous payez un abonnement en fonction du nombre maximum d'utilisateurs affectés au groupe de premier niveau ou à ses enfants pendant la période de facturation. Vous pouvez ajouter et supprimer des utilisateurs pendant la période d'abonnement, tant que le nombre total d'utilisateurs à un moment donné ne dépasse pas le nombre d'abonnements."
              link:
                href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#how-seat-usage-is-determined
                text: 'En savoir plus'
                ga_name: Determine how many seats you want
                ga_location: body
            - title: 'Obtenir votre abonnement SaaS'
              text: |
                <p>GitLab SaaS est l'offre de logiciel-service de GitLab, disponible sur GitLab.com. Il n'est pas nécessaire d'installer quoi que ce soit pour utiliser GitLab SaaS, il suffit de s'inscrire. L'abonnement détermine les fonctionnalités disponibles pour vos projets privés. Les organisations ayant des projets open source publics peuvent activement postuler à notre programme GitLab pour Open Source.<p/>

                <p>Les fonctionnalités de <a href="/pricing/ultimate/">GitLab Ultimate</a>, y compris 50 000 calculer les minutes, sont gratuites pour les projets open source éligibles via le programme <a href="/solutions/open-source/">GitLab pour Open Source</a> .</p>
              link:
                href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#view-your-gitlabcom-subscription
                text: 'En savoir plus'
                ga_name: Obtain your SaaS subscription
                ga_location: body
            - title: "Déterminer les minutes d'exécution partagée CI/CD nécessaires"
              text: |
                <p><a href="https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners" data-ga-name="Shared Runners" data-ga-location="body">Runners partagés</a> sont partagés avec chaque projet et groupe dans une instance GitLab. Lorsque des travaux s'exécutent sur des runners partagés, des calculer les minutes sont utilisées. Sur GitLab.com, le quota d'calculer les minutes est défini pour chaque <a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">namespace</a>, et est déterminé par <a href="/pricing/" data-ga-name="Your license tier" data-ga-location="body">votre niveau de licence.</a><p/>

                <p>En plus du quota mensuel, sur GitLab.com, vous pouvez <a href="https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes" data-ga-name="purchase additional compute minutes" data-ga-location="body">acheter des calculer les minutes supplémentaires</a> lorsque vous en avez besoin.</p>
        - title: "Créez votre compte d'abonnement autogéré"
          copies:
            - title: 'Déterminez le nombre de sièges que vous souhaitez'
              text: |
                Un abonnement GitLab SaaS utilise un modèle concurrent (siège). Vous payez un abonnement en fonction du nombre maximum d'utilisateurs affectés au groupe de premier niveau ou à ses enfants pendant la période de facturation. Vous pouvez ajouter et supprimer des utilisateurs pendant la période d'abonnement, tant que le nombre total d'utilisateurs à un moment donné ne dépasse pas le nombre d'abonnements.
              link:
                href: https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-seats
                text: En savoir plus
                ga_name: Determine how many seats you want
                ga_location: body
            - title: 'Obtenir votre abonnement autogéré'
              text: |
                Vous pouvez installer, administrer et maintenir votre propre instance de GitLab. La gestion des abonnements à GitLab nécessite l'accès au portail des clients.
              link:
                href: https://docs.gitlab.com/ee/subscriptions/self_managed/
                text: 'En savoir plus'
                ga_name: Determine how many seats you want
                ga_location: body
            - title: 'Activer GitLab Enterprise Edition'
              text: |
                Lorsque vous installez une nouvelle instance de GitLab sans licence, seules les fonctionnalités gratuites sont activées. Pour activer plus de fonctionnalités dans GitLab Enterprise Edition (EE), activez votre instance avec un code d'activation.
              link:
                href: https://docs.gitlab.com/ee/user/admin_area/license.html
                text: 'En savoir plus'
                ga_name: Activate GitLab Enterprise Edition
                ga_location: body
            - title: 'Examiner les exigences du système'
              text: |
                Passez en revue les systèmes d'exploitation pris en charge et les exigences minimales requises pour installer et utiliser GitLab.
              link:
                href: https://docs.gitlab.com/ee/install/requirements.html
                text: 'En savoir plus'
                ga_name: Review the system requirements
                ga_location: body
            - title: 'Installer GitLab'
              text: |
                <p>Choisissez votre méthode d'installation <a href="https://docs.gitlab.com/ee/install/#choose-the-installation-method" data-ga-name="Installation Method" data-ga-location="body"></a></p>

                <p>Installez sur <a href="https://docs.gitlab.com/ee/install/#install-gitlab-on-cloud-providers" data-ga-name="your cloud provider" data-ga-location="body">votre fournisseur de cloud</a> (si applicable)</p>
            - title: 'Configurez votre instance'
              link:
                href: https://docs.gitlab.com/ee/install/next_steps.html
                text: 'En savoir plus'
                ga_name: Configure your instance
                ga_location: body
            - title: "Mise en place d'un environnement hors ligne"
              text: |
                Mise en place d'un environnement hors ligne lorsqu'il est nécessaire de s'isoler de l'internet public (généralement applicable aux industries réglementées)
              link:
                href: https://docs.gitlab.com/ee/user/application_security/offline_deployments/index.html
                text: 'En savoir plus'
                ga_name: Set up off-line environment
                ga_location: body
            - title: 'Envisager de limiter le nombre de minutes autorisées pour les exécutants partagés CI/CD'
              text: |
                Pour contrôler l'utilisation des ressources sur les instances GitLab autogérées, le quota d'calculer les minutes pour chaque espace de noms peut être défini par les administrateurs.
              link:
                href: https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-quota-of-cicd-minutes-for-a-specific-namespace
                text: 'En savoir plus'
                ga_name: Consider limiting CI/CD shared runner minutes allowed
                ga_location: body
            - title: 'Installer le runner GitLab'
              text: |
                GitLab Runner peut être installé et utilisé sur GNU/Linux, macOS, FreeBSD et Windows.
              link:
                href: https://docs.gitlab.com/runner/install/
                text: 'En savoir plus'
                ga_name: Install GitLab runner
                ga_location: body
            - title: 'Configurer le runner GitLab (optionnel)'
              text: |
                GitLab Runner peut être installé et utilisé sur GNU/Linux, macOS, FreeBSD et Windows.
              link:
                href: https://docs.gitlab.com/runner/configuration/
                text: 'En savoir plus'
                ga_name: Configure GitLab runner
                ga_location: body
            - title: 'Administration'
              text: |
                L'autogestion nécessite une auto-administration
              link:
                href: https://docs.gitlab.com/ee/administration/
                text: 'En savoir plus'
                ga_name: Self Administration
                ga_location: body
        - title: Intégrer des applications (en option
          copies:
            - text: |
                Vous pouvez ajouter des fonctionnalités telles que la gestion des secrets ou les services d'authentification, ou intégrer des applications existantes telles que les systèmes de suivi des problèmes.
              link:
                href: https://docs.gitlab.com/ee/integration/
                text: 'En savoir plus'
                ga_name: Integrate applications
                ga_location: body
    - header: 'Utiliser GitLab'
      id: 'using-gitlab'
      items:
        - title: "Mise en place de l'organisation"
          copies:
            - text: |
                Configurez votre organisation et ses utilisateurs. Déterminez les rôles des utilisateurs et donnez à chacun l'accès aux projets dont il a besoin.
              link:
                href: https://docs.gitlab.com/ee/topics/set_up_organization.html
                text: 'En savoir plus'
                ga_name: Setup your organization
                ga_location: body
        - title: 'Organiser le travail en fonction des projets'
          copies:
            - text: |
                Dans GitLab, vous pouvez créer des projets pour héberger votre base de code. Vous pouvez également utiliser des projets pour suivre les problèmes, planifier le travail, collaborer sur le code et construire, tester et utiliser le système CI/CD intégré pour déployer votre application.
              link:
                href: https://docs.gitlab.com/ee/user/project/index.html
                text: 'En savoir plus'
                ga_name: Organize work with projects
                ga_location: body
        - title: 'Planifier et suivre le travail'
          copies:
            - text: |
                Planifiez votre travail en créant des exigences, des questions et des thèmes. Planifiez le travail avec des étapes et suivez le temps passé par votre équipe. Apprenez à gagner du temps avec des actions rapides, voyez comment GitLab rend le texte Markdown, et apprenez à utiliser Git pour interagir avec GitLab.
              link:
                href: https://docs.gitlab.com/ee/topics/plan_and_track.html
                text: 'En savoir plus'
                ga_name: Plan and track work
                ga_location: body
        - title: 'Créez votre application'
          copies:
            - text: |
                Ajoutez votre code source à un référentiel, créez des demandes de fusion pour vérifier le code et utilisez CI/CD pour générer votre application.
              link:
                href: https://docs.gitlab.com/ee/topics/build_your_application.html
                text: 'En savoir plus'
                ga_name: Build your application
                ga_location: body
        - title: 'Sécurisez votre demande'
          copies:
            - title: Déterminez les scanners que vous souhaitez utiliser (tous sont activés par défaut).
              text: |
                GitLab propose à la fois l'analyse des conteneurs et l'analyse des dépendances pour assurer la couverture de tous ces types de dépendances. Pour couvrir la plus grande partie possible de votre zone de risque, nous vous encourageons à utiliser tous nos scanners de sécurité.
              link:
                href: https://docs.gitlab.com/ee/user/application_security/configuration/
                text: 'En savoir plus'
                ga_name: Determine which scanners you’d like to use
                ga_location: body
            - title: 'Configurez vos politiques de sécurité'
              text: |
                Les politiques de GitLab permettent aux équipes de sécurité d'exiger que les analyses de leur choix soient exécutées chaque fois qu'un pipeline de projet s'exécute conformément à la configuration spécifiée. Les équipes de sécurité peuvent donc être sûres que les analyses qu'elles ont mises en place n'ont pas été modifiées, altérées ou désactivées.
              link:
                href: https://docs.gitlab.com/ee/user/application_security/policies/
                text: 'En savoir plus'
                ga_name: Configure your security policies
                ga_location: body
            - title: "Configurer les règles d'approbation MR et les approbations de sécurité"
              text: |
                Les règles d'approbation des demandes de fusion vous permettent de définir le nombre minimum d'approbations requises avant qu'un travail puisse être fusionné dans votre projet. Vous pouvez également étendre ces règles pour définir quels types d'utilisateurs peuvent approuver le travail.
              link:
                href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
                text: 'En savoir plus'
                ga_name: Configure MR approval rules and security approvals
                ga_location: body
        - title: 'Déployer et publier votre application'
          copies:
            - text: |
                Déployez votre application en interne ou au public. Utiliser des drapeaux pour publier des fonctionnalités de manière incrémentale.
              link:
                href: https://docs.gitlab.com/ee/topics/release_your_application.html
                text: 'En savoir plus'
                ga_name: Deploy and release your application
                ga_location: body
        - title: "Contrôler les performances de l'application"
          copies:
            - text: |
                GitLab fournit une variété d'outils pour aider à opérer et maintenir vos applications.  Vous pouvez suivre les mesures les plus importantes pour votre équipe, générer des alertes automatiques en cas de dégradation des performances et gérer ces alertes, le tout dans GitLab.
              link:
                href: https://docs.gitlab.com/ee/operations/index.html
                text: 'En savoir plus'
                ga_name: Monitor application performance
                ga_location: body
        - title: 'Contrôler les performances des coureurs'
          copies:
            - text: |
                Depuis GitLab 8.4, GitLab dispose de son propre système de mesure de la performance des applications, appelé "GitLab Performance Monitoring". GitLab Performance Monitoring permet de mesurer une grande variété de statistiques
              link:
                href: https://docs.gitlab.com/runner/monitoring/index.html
                text: 'En savoir plus'
                ga_name: Monitor runner performance
                ga_location: body
        - title: 'Gérer votre infrastructure'
          copies:
            - text: |
                Avec l'essor des approches DevSecOps et SRE, la gestion de l'infrastructure devient codifiée, automatisable, et les meilleures pratiques de développement logiciel gagnent également leur place autour de la gestion de l'infrastructure. GitLab offre diverses fonctionnalités pour accélérer et simplifier vos pratiques de gestion de l'infrastructure.
              link:
                href: https://docs.gitlab.com/ee/user/infrastructure/index.html
                text: 'En savoir plus'
                ga_name: Manage your infrastructure
                ga_location: body
        - title: "Analyser l'utilisation de GitLab"
          copies:
            - text: |
                Il s'agit de mesurer la fréquence à laquelle vous apportez de la valeur aux utilisateurs finaux. Une fréquence de déploiement plus élevée permet d'obtenir un retour d'information plus rapide et d'apporter des améliorations et des fonctionnalités plus rapidement.
              link:
                href: https://docs.gitlab.com/ee/user/analytics/index.html
                text: 'En savoir plus'
                ga_name: Analyze GitLab usage
                ga_location: body
next_steps:
  header: "Faites passer votre entreprise à l'étape suivante"
  cards:
    - title: 'Avez-vous un volet payant?'
      text: "Vous bénéficiez du soutien d'un gestionnaire de comptes techniques (TAM)."
      avatar: /nuxt-images/icons/avatar_orange.png
      col_size: 4
      link:
        text: Que mon TAM me contacte
        url: /sales/
        data_ga_name: Have my TAM contact me
        data_ga_location: body
    - title: "Besoin d'aide?"
      text: 'Les services professionnels de GitLab peuvent vous aider à démarrer, à intégrer des applications tierces, etc.'
      avatar: /nuxt-images/icons/avatar_pink.png
      col_size: 4
      link:
        text: 'Que mon PS me contacte'
        url: /sales/
        data_ga_name: Have my PS contact me
        data_ga_location: body
    - title: 'Vous préférez travailler avec un partenaire de distribution?'
      avatar: /nuxt-images/icons/avatar_blue.png
      col_size: 4
      link:
        text: "Voir l'annuaire des partenaires de distribution"
        url: /sales/
        data_ga_name: See channel partner directory
        data_ga_location: body
    - title: 'Vous envisagez une mise à niveau?'
      text: |
        "Découvrez les avantages de [Premium](/pricing/premium/){data-ga-name="why premium" data-ga-location="body"} et [Ultimate](/pricing/ultimate/){data-ga-name="why ultimate" data-ga-location="body"}."
      col_size: 6
      link:
        text: "Voir les détails de l'échelonnement"
        url: /sales/
        data_ga_name: See tiering details
        data_ga_location: body
    - title: "Vous envisagez l'intégration d'un tiers?"
      text: 'Les services professionnels de GitLab peuvent vous aider à démarrer, à intégrer des applications tierces, etc.'
      col_size: 6
      link:
        text: Voir nos partenaires Alliance et Technologie
        url: /partners/
        data_ga_name: See our Alliance and Technology partners
        data_ga_location: body
