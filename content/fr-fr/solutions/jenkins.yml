---
  title: Intégration Jenkins de GitLab
  description: L'intégration Jenkins de GitLab permet de configurer sans effort votre projet à compiler avec Jenkins. GitLab produira les résultats pour vous directement à partir de l'interface utilisateur (UI) de GitLab.
  components:
    - name: 'solutions-hero'
      data:
        title: Intégration Jenkins de GitLab
        subtitle: Déclenchez une compilation Jenkins pour chaque poussée vers vos projets GitLab
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: https://docs.gitlab.com/ee/integration/jenkins.html
          text: Documentation
          data_ga_name: jenkins integration
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: intégration Jenkins de GitLab"
    - name: copy
      data:
        block:
          - header: Présentation
            id: overview
            text: |
              GitLab est une plateforme de développement logiciel complète qui comprend, entre autres puissantes [fonctionnalités](/features/){data-ga-name="features" data-ga-location="body"}, [GitLab CI/CD](/solutions/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"} intégré pour tirer parti de la capacité à compiler, tester et déployer vos applications sans vous obliger à intégrer des outils externes de CI/CD.

              De nombreuses entreprises utilisent toutefois [Jenkins](https://jenkins.io/) pour leurs processus de déploiement et ont besoin d'une intégration avec Jenkins pour pouvoir s'intégrer à GitLab avant de passer à GitLab CI/CD. D'autres doivent utiliser Jenkins pour compiler et déployer leurs applications en raison de l'incapacité à modifier l'infrastructure établie pour les projets en cours, mais souhaitent utiliser GitLab pour toutes les autres fonctionnalités.

              Avec l'intégration Jenkins de GitLab, vous pouvez configurer sans effort votre projet à compiler avec Jenkins, et GitLab produira les résultats pour vous directement à partir de l'UI de GitLab.
    - name: copy-media
      data:
        block:
          - header: Fonctionnement
            id: how-it-works
            inverted: true
            text: |
              * **Afficher les résultats Jenkins sur les requêtes de fusion GitLab :** Lorsque vous configurez l'intégration Jenkins de GitLab pour votre projet, toute poussée vers votre projet déclenchera une compilation sur l'installation externe Jenkins, et GitLab affichera le statut du pipeline (réussi ou échec) pour vous directement sur le widget de la requête de fusion et dans la liste des pipelines de votre projet.
              * **Accédez rapidement à vos journaux de compilation :** Chaque fois que vous souhaitez vérifier votre journal de compilation, il vous suffit de cliquer sur le badge de résultat et GitLab vous dirigera vers votre [pipeline](https://docs.gitlab.com/ee/ci/pipelines/index.html){data-ga-name="pipeline" data-ga-location="body"} sur l'UI Jenkins.
            icon:
              name: checklist
              alt: Liste de contrôle
              variant: marketing
              hex_color: '#F43012'
          - header: Avantages
            id: benefits
            text: |
              * **Facilement et rapidement configurable :** Jenkins s'intègre facilement à [GitLab Enterprise Edition](/pricing/){data-ga-name="pricing" data-ga-location="body"}, directement à partir des paramètres d'intégration de votre projet. Une fois que vous avez activé le service pour configurer l'authentification de GitLab avec votre serveur Jenkins, et que Jenkins sait comment interagir avec GitLab, il est prêt à l'emploi, directement.
              * **Maintenez votre flux de travail optimisé avec GitLab :** Même si Jenkins exécute vos compilations, tout le reste peut être géré par GitLab, depuis la discussion de nouvelles idées jusqu'au déploiement en production. L'utilisation de l'interface Jenkins n'est nécessaire que si vous souhaitez obtenir plus de détails, par exemple en cas d'échec.
            icon:
              name: clock-alt
              alt: Horloge
              variant: marketing
              hex_color: '#F43012'
