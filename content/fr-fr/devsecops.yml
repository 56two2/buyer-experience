---
  title: Pourquoi utiliser GitLab ?
  description: "Ne nous croyez pas seulement sur parole\_! GitLab est la seule plateforme DevOps complète proposée sous la forme d'une application unique. En savoir plus ici\_!"
  hero:
    header: Les leaders du secteur choisissent GitLab
    description: GitLab est le seul endroit où les entreprises créent des logiciels critiques.
    primaryButton:
      text: Essayer Ultimate gratuitement
      href: https://gitlab.com/-/trial_registrations/new?_gl=1%2A1lt5wks%2A_ga%2AMTc4NzA4MjI4Ni4xNjc1OTYwMzk0%2A_ga_ENFH3X7M5Y%2AMTY4MTE0OTk0MC4xMjkuMS4xNjgxMTUwMTMwLjAuMC4w&glm_content=default-saas-trial&glm_source=about.gitlab.com%2Fpricing%2F
    video:
      title: Qu'est-ce que GitLab ?
      url: https://player.vimeo.com/video/799236905?h=4eee39a447&badge=0&autopause=0&player_id=0&app_id=58479
      image: /nuxt-images/solutions/landing/solutions_video_thumbnail.png
  categories_block:
    header: Trois raisons pour lesquelles
    organization: GitLab
    footer:
      - Découvrez pourquoi
      - et
      - les experts préfèrent GitLab
    categories:
      - title: Développeurs
        bullets:
          - title: Application unique
            description: GitLab regroupe toutes les fonctionnalités DevSecOps dans une seule application avec un magasin de données unifié pour que tout soit au même endroit.
          - title: Amélioration de la productivité
            description: L'application unique de GitLab offre une meilleure expérience à l'utilisateur, ce qui améliore le durée de cycle et permet d'éviter le changement de contexte.
          - title: Une meilleure automatisation, là où cela compte vraiment
            description: Les outils d'automatisation de GitLab sont plus fiables et plus riches en fonctionnalités, ce qui permet d'éliminer la charge cognitive et les tâches inutiles.
        customer:
          logo: /nuxt-images/logos/deakin-university-logo.svg
          text: Découvrez comment l'université Deakin a réduit les tâches manuelles de 60 % avec GitLab.
          button:
            text: En savoir plus
            href: /customers/deakin-university/
            dataGaName: deakin
        cta:
          icon: time-is-money
          title: Votre plateforme DevSecOps automatise-t-elle les tâches qui améliorent l'efficacité de l'équipe et réduisent le temps de codage des développeurs ?
          text: Les fonctions d'automatisation sont une composante essentielle d'une plateforme DevSecOps complète qui garantit l'efficacité, l'évolutivité, la sécurité et la conformité, en plus de la création de code.
          button:
            text: Calculez vos économies potentielles avec notre outil de retour sur investissement (ROI)
            href: /calculator/roi/
            dataGaName: roi calculator
      - title: Sécurité
        bullets:
          - title: La sécurité est intégrée, elle n'est pas greffée
            description: Les fonctionnalités de sécurité de GitLab (DAST, test à données aléatoires, analyse des conteneurs et vérification des API) sont intégrées de bout en bout.
          - title: Conformité et gestion précise des politiques
            description: GitLab offre une solution de gouvernance complète permettant la séparation des tâches entre les équipes. L'éditeur de politique de GitLab permet des règles d'approbation personnalisées adaptées aux exigences de conformité de chaque organisation, réduisant ainsi les risques.
          - title: Automatisation de la sécurité
            description: Les outils d'automatisation avancés de GitLab permettent de gagner en rapidité grâce à des garde-fous qui garantissent que le code est automatiquement analysé pour détecter les vulnérabilités.
        customer:
          logo: /nuxt-images/logos/hackerone-logo.png
          text: Découvrez comment l'équipe d'ingénieurs de HackerOne a utilisé l'automatisation avec GitLab pour réduire la durée de cycle manuel et créer des analyses de sécurité plus rapides, économisant une heure supplémentaire par déploiement sur les tests.
          button:
            text: En savoir plus
            href: /customers/hackerone/
            dataGaName: hackerone
        cta:
          icon: shield-check-light
          title: Votre plateforme est-elle en mesure d'intégrer la sécurité tout au long du cycle de vie du logiciel ?
          text: L'intégration de la sécurité à chaque étape réduit le besoin d'intégrations supplémentaires et minimise le risque d'échec
          button:
            text: En savoir plus sur notre engagement en matière de sécurité de l'information
            href: /security/
            dataGaName: security
      - title: Opérations
        bullets:
          - title: Faites évoluer les charges de travail de votre entreprise
            description: GitLab prend facilement en charge l'entreprise à n'importe quelle échelle avec la possibilité de gérer et de mettre à niveau avec un temps d'arrêt quasi nul.
          - title: Visibilité inégalée des métriques
            description: Le magasin de données unifié de GitLab fournit des analyses pour l'ensemble du cycle de vie du développement logiciel en un seul endroit, éliminant ainsi le besoin d'intégrations de produits supplémentaires.
          - title: Pas d'enfermement du Cloud
            description: GitLab n'est pas lié commercialement à un seul fournisseur de services Cloud, ce qui élimine le risque d'enfermement propriétaire.
        customer:
          logo: /nuxt-images/home/logo_iron_mountain_mono.svg
          text: Découvrez comment Iron Mountain a réduit les coûts de gestion de l'infrastructure et augmenté la vitesse de production en toute sécurité avec GitLab, ce qui a permis d'économiser plus de 150 000 dollars par an et de réduire de près de moitié le nombre de machines virtuelles sur site.
          button:
            text: En savoir plus
            href: /customers/iron-mountain/
            dataGaName: iron mountain
        cta:
          icon: time-is-money
          title: 'Coût total de possession : votre plateforme DevSecOps peut-elle évoluer efficacement sans entraîner de coûts excessifs ?'
          text: Les outils supplémentaires requis par certains fournisseurs peuvent rapidement atteindre des coûts exorbitants, tant en frais de gestion et de maintenance qu'en frais financiers, au fur et à mesure que votre entreprise se développe.
          button:
            text: Contacter un expert
            href: /sales/
            dataGaName: sales
  badges:
    header: Adoré par les développeurs. <br> Approuvé par les entreprises.
    description: GitLab se classe parmi les leaders au Classement G2 dans les catégories DevOps.
    badges:
    - src: /nuxt-images/devsecops/badge1.svg
      alt: G2 Meilleurs résultats - Printemps 2023
    - src: /nuxt-images/devsecops/badge2.svg
      alt: G2 Entreprise Leader - Printemps 2023
    - src: /nuxt-images/devsecops/badge3.svg
      alt: G2 Le plus facile à utiliser - Printemps 2023
    - src: /nuxt-images/devsecops/badge4.svg
      alt: G2 Meilleur relationnel Entreprise - Printemps 2022
    - src: /nuxt-images/devsecops/badge5.svg
      alt: G2 Plus haut taux d'adoption par les utilisateurs - Printemps 2022
    - src: /nuxt-images/devsecops/badge6.svg
      alt: G2 La plus facile à mettre en œuvre - Printemps 2022
  pricing:
    header: Commencez à livrer des logiciels plus rapidement
    ctaText: En savoir plus sur les tarifs
    learnMoreText: En savoir plus sur
    buyText: Acheter
    tiers:
      - name: Premium
        purchaseUrl: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities&_gl=1*7pkvar*_ga*MjE0NDM3MDYyNi4xNjY1NjI1NDM4*_ga_ENFH3X7M5Y*MTY4MzU4MTkzNS4zMDguMS4xNjgzNTgzNzY1LjAuMC4w
        learnMoreUrl: /pricing/premium/
        benefits:
          - Propriété du code et branches protégées
          - Requêtes de fusion avec des règles d'approbation
          - Planification agile d'entreprise
        ctaPrimary: Acheter GitLab Premium
        ctaSecondary: En savoir plus sur Premium
      - name: Ultimate
        purchaseUrl: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities&_gl=1*cwydal*_ga*MjE0NDM3MDYyNi4xNjY1NjI1NDM4*_ga_ENFH3X7M5Y*MTY4MzU4MTkzNS4zMDguMS4xNjgzNTgzNzc4LjAuMC4w
        learnMoreUrl: /pricing/ultimate/
        benefits:
          - Test dynamique de sécurité des applications (DAST)
          - Tableaux de bord de sécurité
          - Gestion des vulnérabilités
          - Analyse des dépendances
          - Analyse de conteneurs
        ctaPrimary: Acheter GitLab Ultimate
        ctaSecondary: En savoir plus sur  Ultimate
    footnote: Vous n'êtes pas encore prêt à passer au forfait [Premium](/pricing/premium/){data-ga-name="premium note" data-ga-location="pricing tier blocks"} ni au forfait [Ultimate](/pricing/ultimate/){data-ga-name="ultimate note" data-ga-location="pricing tier blocks"} ? Vous pouvez toujours vous inscrire et utiliser notre [forfait Gratuit](https://gitlab.com/-/trial_registrations/new?_gl=1%2A1lt5wks%2A_ga%2AMTc4NzA4MjI4Ni4xNjc1OTYwMzk0%2A_ga_ENFH3X7M5Y%2AMTY4MTE0OTk0MC4xMjkuMS4xNjgxMTUwMTMwLjAuMC4w&glm_content=default-saas-trial&glm_source=about.gitlab.com%2Fpricing%2F){data-ga-name="free note" data-ga-location="pricing tier blocks"}.
