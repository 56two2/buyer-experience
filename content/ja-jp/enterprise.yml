---
  title: GitLab for Enterprise  - スムーズなコラボレーションが可能に
  description: GitLab DevSecOpsプラットフォームを使用してエンタープライズソフトウェアのデリバリーを加速し、開発コストの削減、チームのコラボレーションの合理化を実現
  image_title: /nuxt-images/open-graph/open-graph-enterprise.png
  side_navigation_links:
    - title: 概要
      href: '#overview'
    - title: メリット
      href: '#benefits'
    - title: 機能
      href: '#capabilities'
    - title: ケーススタディ
      href: '#case-studies'
  solutions_hero:
    title: エンタープライズ向け GitLab
    subtitle: 計画から本番までをカバーする、最も包括的なDevSecOpsプラットフォーム。 組織全体での連携を高め、セキュアなコードを速やかにデプロイし、ビジネスの成果を推進します。
    header_animation: fade-down
    header_animation_duration: 800
    buttons_animation: fade-down
    buttons_animation_duration: 1200
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: 無料トライアルを開始
      url: https://gitlab.com/-/trial_registrations/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: free trial
      data_ga_location: header
    secondary_btn:
      text: お問い合わせ
      url: /sales
      data_ga_name: talk to an expert
      data_ga_location: header
    image:
      image_url: /nuxt-images/enterprise/enterprise-header.jpeg
      alt: "車に積む箱"
      rounded: true
  by_industry_intro:
    logos:
      - name: Siemens
        image: /nuxt-images/logos/logo_siemens_color.svg
        aos_animation: zoom-in-up
        aos_duration: 200
        url: /customers/siemens/
        aria_label: Siemensお客様事例へのリンク
      - name: hilti
        image: /nuxt-images/customers/hilti-logo.png
        aos_animation: zoom-in-up
        aos_duration: 400
        url: /customers/hilti/
        aria_label: Hiltiお客様事例へのリンク
      - name: Bendigo
        image: /nuxt-images/customers/babl_logo.png
        aos_animation: zoom-in-up
        aos_duration: 600
        url: /customers/bab/
        aria_label: Bendigoお客様事例へのリンク
      - name: Radio France
        image: /nuxt-images/logos/logoradiofrance.svg
        aos_animation: zoom-in-up
        aos_duration: 800
        url: /customers/radiofrance/
        aria_label: Radio Franceお客様事例へのリンク
      - name: Credit agricole
        image: /nuxt-images/customers/credit-agricole-logo-1.png
        aos_animation: zoom-in-up
        aos_duration: 1000
        url: /customers/credit-agricole/
        aria_label: Credit Agricoleお客様事例へのリンク
      - name: Kiwi
        image: /nuxt-images/customers/kiwi.png
        aos_animation: zoom-in-up
        aos_duration: 1200
        url: /customers/kiwi/
        aria_label: Kiwiお客様事例へのリンク
  by_solution_intro:
    aos_animation: fade-up
    aos_duration: 800
    text:
      highlight: 大規模な組織は、DevSecOpsを実践することで、スピーディーなソフトウェアリリースを実現しています。
      description: 個々のプロジェクトでうまく機能したものを、企業全体にスケールするのは簡単ではありません。異なるツールを個別に導入して構築された脆弱なツールチェーンとは異なり、GitLabは組織全体の迅速な進化とコラボレーションを実現します。複雑さとリスクを排除しながら、高品質でセキュアなソフトウェアを迅速に提供します。
  by_solution_benefits:
    title: 大規模なDevSecOps
    is_accordion: true
    right_block_animation: zoom-in-left
    right_block_duration: 800
    left_block_animation: zoom-in-right
    left_block_duration: 800
    header_animation: fade-up
    header_duration: 800
    image:
      image_url: /nuxt-images/enterprise/enterprise-devops-at-scale.jpg
      alt: "コラボレーションイメージ"
    items:
      - icon:
          name: increase
          alt: 増加アイコン
          variant: marketing
        header: 生産性の高いコラボレーション
        text: ClickOpsを排除し、クラウドネイティブの導入をシームレスに調整します。
      - icon:
          name: gitlab-release
          alt: GitLabリリースアイコン
          variant: marketing
        header: リスクとコストの削減
        text: テスト頻度を高め、エラーを瞬時に検出し、リスクを最小限に抑えます。
      - icon:
          name: collaboration
          alt: コラボレーション アイコン
          variant: marketing
        header: より優れたソフトウェアをより速く提供
        text: 付加価値の高いタスクに焦点を当て、ルーチン作業を簡素化します。
      - icon:
          name: release
          alt: アジャイルアイコン
          variant: marketing
        header: DevSecOpsを簡素化
        text: すべてのDevSecOpsプロセスを一元管理することで、設定や構成の複雑さを最小限に抑えながら成功をスケールアップできます。
  by_industry_solutions_block:
    subtitle: 公共部門向けの完全なDevSecOpsプラットフォーム
    sub_description: "セキュアで堅牢なソースコード管理(SCM)、継続的インテグレーション(CI)、継続的デリバリー(CD)、継続的なソフトウェアセキュリティとコンプライアンスを備えた単一のDevSecOpsプラットフォームを駆使し、GitLabは次のような固有のニーズにも対応可能です。"
    white_bg: true
    sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec-ja.svg
    alt: メリットイメージ
    solutions:
      - title: アジャイル計画
        description: 革新的なプロジェクトの計画と管理をサポートし、組織内での作業内容や進捗状況を可視化します。
        icon:
          name: release
          alt: アジャイルアイコン
          variant: marketing
        link_text: 詳細はこちら
        link_url: /solutions/agile-delivery
        data_ga_name: agile planning
        data_ga_location: body
      - title: 自動化されたソフトウェアデリバリー
        description: プロジェクトのソフトウェア部品表（SBOM）を確認し、使用されている依存関係とそれらの既知の脆弱性についての重要な詳細情報をチェックします。
        icon:
          name: automated-code
          alt: 自動コードアイコン
          variant: marketing
        link_text: 詳細はこちら
        link_url: /solutions/delivery-automation/
        data_ga_name: Automated Software Delivery
        data_ga_location: body
      - title: 継続的なセキュリティとコンプライアンス
        description: 開発プロセス全体でセキュリティをシフトレフトし、コンプライアンスを自動化することで、リスクと遅延を軽減します。
        icon:
          name: devsecops
          alt: DevSecOpsアイコン
          variant: marketing
          link_text: 詳細はこちら
        link_url: /solutions/continuous-software-compliance/
        data_ga_name: Continuous Security & Compliance
        data_ga_location: body
      - title: バリューストリーム管理
        description: 組織内のすべてのステークホルダーに実行可能な洞察を提供し、アイデアの概念的なレベルからその後の開発段階まで、すべてのステージを見える化します。
        icon:
          name: visibility
          alt: 可視化アイコン
          variant: marketing
        link_text: 詳細はこちら
        link_url: /solutions/value-stream-management
        data_ga_name: Value Stream Management
        data_ga_location: body
      - title: 信頼性
        description: 地理的に分散しているチームは、ウォームスタンバイ構成を持つGitLab Geoを災害復旧戦略の一環として使用します。災害時においても影響を受けることなく世界中のユーザーが高速かつ効率的に作業しています。
        icon:
          name: remote-world
          alt: リモートの世界アイコン
          variant: marketing
      - title: 高可用性を大規模に実現
        description: 50,000ユーザーを超える高可用性を実現したリファレンスアーキテクチャを参考にしていただけます。
        icon:
          name: auto-scale
          alt: オートスケールアイコン
          variant: marketing
        link_text: 詳細はこちら
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/
        data_ga_name: High availability
        data_ga_location: body
  by_solution_value_prop:
    title: 開発、SEC、運用のための1つのプラットフォーム
    header_animation: fade-up
    header_animation_duration: 500
    cards_animation: zoom-in-up
    cards_animation_duration: 500
    cards:
      - title: 包括的
        description: 作業中のシステム内で、プラットフォーム全体のアナリティクスを使用し、DevSecOpsライフサイクル全体を視覚化し最適化できます。
        icon:
          name: digital-transformation
          alt: デジタルトランスフォーメーションアイコン
          variant: marketing
      - title: DevSecOpsを簡素化
        description: スムーズな運用を阻害しかねないサードパーティのプラグインやAPIに依存せずに、チームやライフサイクルステージ間で共通のツールを使用できます。
        icon:
          name: devsecops
          alt: DevSecOpsアイコン
          variant: marketing
      - title: セキュア
        description: コミットする度に、脆弱性やコンプライアンス違反がないかスキャンします。
        icon:
          name: eye-magnifying-glass
          alt: 虫眼鏡アイコン
          variant: marketing
      - title: 透明性とコンプライアンス
        description: 計画からコードの変更、承認まで、すべてのアクションを自動的に記録して関連付けることで、監査や振り返りで簡単に追跡できます。
        icon:
          name: release
          alt: シールドアイコン
          variant: marketing
      - title: スケールしやすい
        description: 50,000ユーザーを超える、高可用性を実現したスケール方法を示すリファレンスアーキテクチャが公開されています。
        icon:
          name: monitor-web-app
          alt: ウェブアプリのモニタリングアイコン
          variant: marketing
      - title: スケーラブル
        description: KubernetesクラスターにGitLabをデプロイし、スケールアウトします。アップグレードによるダウンタイムはありません。 GitOpsワークフローまたはCI/CDワークフローを使用できます。
        icon:
          name: auto-scale
          alt: auto scale Icon
          variant: marketing
  by_industry_case_studies:
    title: お客様が実感したGitLabの価値
    charcoal_bg: true
    link:
      text: すべての事例
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Siemens
        subtitle: Siemens社が GitLabを使用してオープンソースのDevSecOpsカルチャーを築いた秘訣
        image:
          url: /nuxt-images/blogimages/siemenscoverimage_casestudy.jpg
          alt: 会社のミーティング
        button:
          href: /customers/siemens/
          text: 詳細はこちら
          data_ga_name: siemens learn more
          data_ga_location: body
      - title: Hilti
        subtitle: Hilti社の成功事例：遠隔地からのSDLC強化をもたらすCI/CDとセキュリティスキャン
        image:
          url: /nuxt-images/blogimages/hilti_cover_image.jpg
          alt: 遠くからの建物
        button:
          href: /customers/hilti/
          text: 詳細はこちら
          data_ga_name: hilti learn more
          data_ga_location: body
      - title: Bendigo
        subtitle: GitLabの導入でBendigo社とAdelaide BankがDevSecOpsを加速
        image:
          url: /nuxt-images/blogimages/bab_cover_image.jpg
          alt: ダウンタウンの景色
        button:
          href: /customers/bab/
          text: 詳細はこちら
          data_ga_name: bendigo learn more
          data_ga_location: body
      - title: Radio France
        subtitle: Radio France社は GitLab CI/CDを使用してデプロイ速度を5倍に
        image:
          url: /nuxt-images/blogimages/radio-france-cover-image.jpg
          alt: フランスの景色
        button:
          href: /customers/radiofrance/
          text: 詳細はこちら
          data_ga_name: radio france learn more
          data_ga_location: body
  solutions_resource_cards:
    column_size: 4
    title: リソース
    link:
      text: すべてのリソースを表示
    cards:
      - icon:
          name: webcast
          alt: ウェブキャストアイコン
          variant: marketing
        event_type: ウェビナー
        header: エンドツーエンドのDevOpsプラットフォームで実現：GitLabで問題を最小限にして価値を最大化する
        link_text: 視聴する
        image: /nuxt-images/features/resources/resources_waves.png
        alt: 波
        href: https://www.youtube.com/watch?v=wChaqniv3HI
        aos_animation: fade-up
        aos_duration: 400
      - icon:
          name: webcast
          alt: ウェブキャスト アイコン
          variant: marketing
        event_type: ウェビナー
        header: DevOpsプラットフォームのテクニカルデモ
        link_text: 視聴する
        image: /nuxt-images/features/resources/resources_webcast.png
        alt: カフェで作業
        href: https://youtu.be/Oei67XCnXMk
        aos_animation: fade-up
        aos_duration: 600
      - icon:
          name: event
          alt: ウェブキャストアイコン
          variant: marketing
        event_type: バーチャルイベント
        header: GitLabで達成したNorthwestern Mutual社のデジタルトランスフォーメーション
        link_text: 視聴する
        image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
        alt: gitlab無限のループ
        href: https://www.youtube.com/watch?v=o6EY_WwEFpE
        aos_animation: fade-up
        aos_duration: 800
      - icon:
          name: event
          alt: イベントアイコン
          variant: marketing
        event_type: バーチャルイベント
        header: DevOpsの次のイテレーション(CEO基調講演)
        link_text: 視聴する
        image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
        alt: devopsイメージ
        href: https://www.youtube.com/watch?v=Wx8tDVSeidk
        aos_animation: fade-up
        aos_duration: 1000
      - icon:
          name: case-study
          alt: ケーススタディアイコン
          variant: marketing
        event_type: 事例
        header: Goldman Sachs社は2週間に1回のビルドを1日に1,000回以上に改善
        link_text: 詳細はこちら
        image: /nuxt-images/features/resources/resources_case_study.png
        alt: 上から見た木
        href: /customers/goldman-sachs/
        data_ga_name: Goldman Sachs
        data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
      - icon:
          name: video
          alt: ビデオアイコン
          variant: marketing
        event_type: Video
        header: GitLabインフォマーシャル
        link_text: 視聴する
        image: /nuxt-images/features/resources/resources_golden_dog.png
        alt: 寝ている犬
        href: https://www.youtube.com/embed/gzYTZhJlHoI?
        aos_animation: fade-up
        aos_duration: 1400
