---
  title: GitLabのJenkinsインテグレーション
  description: GitLabのJenkinsとのインテグレーションを使用すれば、プロジェクトを簡単に設定してJenkinsでビルドすることができます。GitLabのUIから直接結果が出力されます。
  components:
    - name: 'solutions-hero'
      data:
        title: GitLabのJenkinsインテグレーション
        subtitle: GitLabプロジェクトへのプッシュごとにJenkinsビルドをトリガーします
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: https://docs.gitlab.com/ee/integration/jenkins.html
          text: ドキュメントはこちら
          data_ga_name: jenkins integration
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像: GitLabのJenkinsとのインテグレーション"
    - name: copy
      data:
        block:
          - header: 概要
            id: overview
            text: |
              GitLabはフル機能を備えたソフトウェア開発プラットフォームです。GitLabで提供される強力な[機能](/features/){data-ga-name="features" data-ga-location="body"}に含まれる、ビルトインの[GitLab CI/CD](/solutions/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"}を使用すると、CI/CDの外部ツールを統合することなく、アプリのビルド、テスト、デプロイを行えます。

              しかし、多くの組織では、デプロイプロセスに[Jenkins](https://jenkins.io/)が使用されているため、GitLabをすぐに使えるようにするには、GitLab CI/CDに切り替える前に、Jenkinsとのインテグレーションが必要となります。また、現在のプロジェクトのために確立されたインフラを変更できないため、アプリケーションのビルドとデプロイにはJenkinsを使用して、他のすべての機能にはGitLabを使いたいという人もいます。

              GitLabのJenkinsインテグレーションを使えば、Jenkinsでビルドするように簡単にプロジェクトをセットアップすることができ、GitLabはGitLabのUIからすぐに結果を出力します。
    - name: copy-media
      data:
        block:
          - header: 仕組み
            id: how-it-works
            inverted: true
            text: |
              * ** GitLabマージリクエストにJenkinsの結果を表示：**プロジェクトにGitLabのJenkinsインテグレーションをセットアップすると、プロジェクトへのプッシュによって外部のJenkinsインストールでのビルドがトリガーされます。GitLabは、プロジェクトのパイプラインリストからマージリクエストウィジェットに直接、パイプラインのステータス(成功または失敗)を出力します。
              * **ビルドログにすばやくアクセス：**ビルドログを確認するたびに、結果バッジをクリックすると、Jenkins UIの[パイプライン](https://docs.gitlab.com/ee/ci/pipelines/index.html){data-ga-name="pipeline" data-ga-location="body"}に移動します。
            icon:
              name: checklist
              alt: チェックリスト
              variant: marketing
              hex_color: '#F43012'
          - header: メリット
            id: benefits
            text: |
              * **簡単かつ迅速に設定可能：** Jenkinsは、プロジェクトのインテグレーション設定から直接、[GitLabエンタープライズエディション](/pricing/){data-ga-name="pricing" data-ga-location="body"}に簡単に統合できます。JenkinsサーバーでGitLabの認証を設定するサービスを有効にして、Jenkins側でGitLabとの接続が認識されたら、すぐに使用できます。
              * ** GitLabを活用して行っているワークフローを維持：** Jenkins側でビルドを実行しているにもかかわらず、新しいアイデアのディスカッションから本番環境へのデプロイまで、他のすべてをGitLabで処理できます。Jenkinsインターフェイスは、失敗が発生した場合など、詳細情報を取得する場合にのみ必要です。
            icon:
              name: clock-alt
              alt: 時計
              variant: marketing
              hex_color: '#F43012'
