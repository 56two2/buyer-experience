template: feature
title: '継続的インテグレーションとデリバリ'
description: 'オンデマンドのソフトウェアデリバリーを繰り返し実現'
components:
  feature-block-hero:
    data:
      title: 継続的インテグレーションとデリバリ
      subtitle: オンデマンドのソフトウェアデリバリを繰り返し実現
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        text: 無料トライアルを開始
        url: /free-trial/
      image:
        image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
        hide_in_mobile: true
        alt: "画像: GitLabの無限ループ"
  side-navigation:
    links:
      - title: 概要
        href: '#overview'
      - title: メリット
        href: '#benefits'
    data:
      feature-overview:
        data:
          content_summary_bolded: GitLabの継続的インテグレーションとデリバリー
          content_summary: >-
            は、コードのビルド、テスト、本番環境へのデプロイに必要なすべてのステップを自動化します。<br><br> <b>継続的インテグレーション</b>は、ビルドの自動化、コードレビューによるフィードバックの提供、およびコード品質テストとセキュリティテストを自動化します。本番環境にすぐにデプロイ可能なリリースパッケージを作成します。<br><br><b>継続的デリバリー</b>は、インフラストラクチャのプロビジョニングや変更管理、チケット発行、リリースバージョン管理を自動化します。これにより、コードのプログレッシブなデプロイ、加えられた変更の検証とモニタリングが可能になるほか、必要に応じてロールバックする機能が提供されます。GitLabの継続的インテグレーションとデリバリーの両方使用することで、マニュアルによる作業を最低限に抑えてソフトウェア開発ライフサイクル(SDLC)を自動化し、オンデマンドで繰り返し実行できます。
          content_image: /nuxt-images/resources/resources_20.jpg
          content_heading: GitLab CI/CDでワークフロー全体を自動化
          content_list:
            - コンテキスト依存テストの結果を使って、デベロッパーの生産性を改善
            - すべての変更をテストすることで、安全でコンプライアンスに沿ったリリースを作成
            - デプロイを保護するための制御を実装
            - シンプルさと拡張性の両方を実現するために、一貫性のあるパイプラインを自動化
            - アプリケーションとインフラストラクチャの両方を自動化
            - クラウドネイティブやハイブリッドクラウドの開発を高速化
      feature-benefit:
        data:
          feature_heading: すべてを自動化
          feature_cards:
            - feature_name: コンテキスト依存テスト
              icon:
                name: automated-code
                variant: marketing
                alt: コードの自動化アイコン
              feature_description: >-
                変更を加えるたびに、自動化されたパイプラインがトリガーされるため、作業が楽になります。
              feature_list:
                - コードやパフォーマンス、負荷、セキュリティなど、すべてをテスト
                - デベロッパーがタスクを切り替える前にマージリクエストで結果を共有
                - コンテキストに沿ってレビュー、連携、イテレーション、および承認を実行
            - feature_name: 安全とコンプライアンス
              icon:
                name: release
                variant: marketing
                alt: チェックマーク付きの盾のアイコン
              feature_description: >-
                コンプライアンスパイプラインから統合されたセキュリティスキャンまで、すべてを1か所で確認できるため、可視性と制御性が向上します。
              feature_list:
                - デベロッパーのワークフローに組み込まれたアプリケーションセキュリティテスト
                - セキュリティの専門家向けの脆弱性管理
                - ポリシーのコンプライアンスを簡素化するための承認、監査レポート、およびトレーサビリティ
            - feature_name: デプロイを保護するための制御を実装
              icon:
                name: approve-dismiss
                variant: marketing
                alt: 承認と却下アイコン
              feature_description: >-
                デプロイを保護するための戦略によって、デプロイの失敗またはパフォーマンスの悪いデプロイを回避します。
              feature_list:
                - コンプライアンスパイプラインによって、一貫してポリシーを遵守していることを確認
                - 機能フラグによってデプロイする対象を細かく制御
                - プログレッシブデリバリー、カナリアデプロイ、ブルーグリーンデプロイを通じてデプロイを行う人を決定
            - feature_name: シンプルさと拡張性の両方を実現するために構築されたパイプライン
              icon:
                name: pipeline-alt
                variant: marketing
                alt: パイプラインアイコン
              feature_description: >-
                開発ライフサイクルのあらゆるステージであなたをサポートする柔軟なパイプライン。
              feature_list:
                - ビルトインのテンプレートを使用して、簡単に開始可能
                - パイプラインはAuto DevOpsにより自動作成
                - 親子パイプラインとマージトレインによってスケール可能
            - feature_name: アプリケーションとインフラストラクチャの自動化
              icon:
                name: devsecops
                variant: marketing
                alt: DevSecOpsループアイコン
              feature_description: >-
                同じプラットフォームでアプリケーションとインフラストラクチャを自動化します。
              feature_list:
                - ライセンス費用と学習曲線を最小化
                - アプリケーションのDevSecOpsのベストプラクティスをインフラストラクチャに活用
            - feature_name: クラウドネイティブやハイブリッドクラウドの開発の高速化
              icon:
                name: continuous-integration
                variant: marketing
                alt: 継続的インテグレーションアイコン
              feature_description: >-
                GitLabはマルチクラウド向けに構築され、インフラストラクチャに依存しないDevSecOpsです。
              feature_list:
                - 仮想マシン、Kubernetesクラスター、さまざまなクラウドベンダーのFaaSへのデプロイをサポート
                - Amazon Web ServicesやGoogle Cloud Platform、Microsoft Azure、独自のプライベートクラウドをサポート
  group-buttons:
    data:
      header:
        text: 継続的インテグレーションにGitLabが役立つ他の方法もご確認ください。
        link:
          text: その他のソリューションを確認
          href: /solutions/
      buttons:
        - text: デリバリーの自動化
          icon_left: automated-code
          href: /solutions/delivery-automation/
        - text: 継続的なソフトウェアセキュリティ
          icon_left: devsecops
          href: /solutions/continuous-software-security-assurance/
        - text: コンプライアンス
          icon_left: shield-check
          href: /solutions/compliance/
  report-cta:
    layout: "dark"
    title: アナリストレポート
    reports:
    - description: "GitLabが『Forrester Wave™: 2023年度第二四半期統合ソフトウェア提供プラットフォーム』において唯一のリーダーに認定"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      link_text: レポートを読む
    - description: "2023年 Gartner® DevOpsプラットフォームのMagic Quadrant™でGitLabがリーダーの1社に位置付け"
      url: /gartner-magic-quadrant/
      link_text: レポートを読む
  solutions-resource-cards:
    data:
      title: リソース
      link:
        text: すべてのリソースを表示
      column_size: 4
      cards:
        - icon:
            name: ebook
            variant: marketing
            alt: 電子書籍アイコン
          event_type: 電子書籍
          header: CI/CDを導入するよう経営陣を説得する方法
          link_text: 詳細はこちら
          image: "/nuxt-images/resources/resources_1.jpeg"
          href: https://page.gitlab.com/2021_eBook_leadershipCICD.html
          data_ga_name: How to convince your leadership to adopt CI/CD
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: 事例アイコン
          event_type: ケーススタディ
          header: >-
            DevSecOpsワークフローでGitLabを活用するAWSのユーザー企業5社
          link_text: 詳細はこちら
          href: https://about.gitlab.com/resources/ebook-five-aws-customers/
          image: /nuxt-images/features/resources/resources_case_study.png
          data_ga_name: Five AWS customers who depend on GitLab for DevSecOps workflow
          data_ga_location: body
        - icon:
            name: webcast
            variant: marketing
            alt: ウェブキャストアイコン
          event_type: ウェブキャスト
          header: GitLab CI/CDの7つのコツ
          link_text: 今すぐ視聴
          href: https://about.gitlab.com/webcast/7cicd-hacks/
          image: /nuxt-images/features/resources/resources_webcast.png
          data_ga_name: 7 GitLab CI/CD hacks
          data_ga_location: body
        - icon:
            name: video
            variant: marketing
            alt: 動画アイコン
          event_type: ビデオ
          header: GitLab CI/CDをはじめる
          link_text: 今すぐ視聴
          href: https://www.youtube.com/embed/sIegJaLy2ug
          image: /nuxt-images/features/resources/resources_webcast.png
          data_ga_name: Getting started with GitLab CI/CD
          data_ga_location: body
        - icon:
            name: video
            variant: marketing
            alt: 動画アイコン
          event_type: ビデオ
          header: CI/CDの概要
          link_text: 今すぐ視聴
          href: https://www.youtube.com/embed/l5705U8s_nQ?start=397
          image: /nuxt-images/resources/fallback/img-fallback-cards-cicd.png
          data_ga_name: CI/CD Overview
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: 事例アイコン
          event_type: ケーススタディ
          header: より高速なパイプラインを実現するためのオートスケーリング
          link_text: 詳細はこちら
          href: https://about.gitlab.com/customers/EAB/
          image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
          data_ga_name: Auto-scaling to achieve faster pipelines
          data_ga_location: body