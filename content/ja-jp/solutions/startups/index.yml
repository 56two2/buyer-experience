---
  title: GitLab for Startups
  description: スタートアップ企業のビジネスを加速する単一のアプリケーション。 基準を満たすスタートアップ企業向けに最高ランクのプランを無料で提供しています。詳細はこちらをご覧ください。
  image_alt: スタートアップ企業向けのGitLab
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  スタートアップ企業向けのGitLabで
        title: 迅速な成長を実現
        subtitle: DevSecOpsプラットフォームを活用して、セキュリティを強化しながらソフトウェアを素早くリリース
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'display1'
        mobile_title_variant: 'heading1-bold'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: https://gitlab.com/-/trial_registrations/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
          text: 無料トライアルを開始
          data_ga_name: Start your free trial
          data_ga_location: header
        secondary_btn:
          url: /solutions/startups/join/
          text: スタートアップ向けGitLabプログラムに参加する
          data_ga_name: startup join
          data_ga_location: header
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/startups_hero.jpeg
          alt: "イメージ: オープンソースのgitlab"
          bordered: true
    - name: customer-logos
      data:
        text_variant: body2
        text: ご利用いただいているお客様
        companies:
        - image_url: "/nuxt-images/logos/chorus-color.svg"
          link_label: Chorusお客様事例へのリンク
          alt: "Chorus ロゴ"
          url: /customers/chorus/
        - image_url: "/nuxt-images/logos/zoopla-logo.png"
          link_label: Zooplaお客様事例へのリンク
          alt: "zooplaロゴ"
          url: /customers/zoopla/
        - image_url: "/nuxt-images/logos/zebra.svg"
          link_label: Zebraお客様事例へのリンク
          alt: "zebraロゴ"
          url: /customers/thezebra/
        - image_url: "/nuxt-images/logos/hackerone-logo.png"
          link_label: Hackeroneお客様事例へのリンク
          alt: "hackeroneロゴ"
          url: /customers/hackerone/
        - image_url: "/nuxt-images/logos/weave_logo.svg"
          link_label: Weave お客様事例へのリンク
          alt: "weaveロゴ"
          url: /customers/weave/
        - image_url: /nuxt-images/logos/inventx.png
          alt: "inventxロゴ"
          url: /customers/inventx/

    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: メリット
          href: '#benefits'
        - title: 顧客
          href: '#customers'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:

            - name: 'solutions-video-feature'
              data:
                header: より速く、効率的に、そして緻密に管理
                description: スタートアップにとって、ソフトウェアをより速く、より効率的にデプロイすることは必要不可欠です。むしろ、顧客層を拡大し、収益目標を達成させて、市場での製品を差別化させる決定的な要因となります。 GitLabは、成長を加速するために構築されたDevSecOpsプラットフォームです。
                video:
                  url: 'https://player.vimeo.com/video/784035583?h=d978ef89bf&color=7759C2&title=0&byline=0&portrait=0'

        - name: 'div'
          id: benefits
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: スタートアップ企業がGitLabを選ぶ理由
                light_background: false
                large_card_on_bottom: true
                cards:
                  - title: ライフサイクル全体で1つのプラットフォーム
                    description: ソフトウェアデリバリーのライフサイクル全体にわたって1つのアプリケーションを使用することで、チームはソフトウェアのリリースに集中できます。多数のツールチェーンの統合に手間取る時間は不要です。コミュニケーションや全体的な可視性、セキュリティの向上や、サイクルタイムの短縮、新メンバーのオンボーディングなど、多くの点において改善が実現します。
                    href: /platform/?stage=plan
                    data_ga_name: platform plan
                    cta: 詳しく見る
                    icon:
                      name: cycle
                      alt: cycle Icon
                      variant: marketing
                  - title: あらゆる環境やクラウドにデプロイ可能
                    description: GitLabはクラウドに依存しないため、お望みの方法でどこでも自由に使用できます。また、企業の成長に合わせてクラウドプロバイダーを柔軟に変更したり追加もできます。AWS、Google Cloud、Azureなどにシームレスにデプロイします。
                    icon:
                      name: merge
                      alt: merge Icon
                      variant: marketing
                  - title: セキュリティのビルトイン
                    description: GitLabには、ソフトウェアデリバリーライフサイクルのすべての段階にセキュリティとコンプライアンスが組み込まれているので、チームは脆弱性をより速く特定し、より迅速かつ効率的な開発が可能です。 企業が成長するにつれて、スピードを犠牲にすることなくリスクを管理できます。
                    href: /solutions/security-compliance/
                    data_ga_name: solutions dev-sec-ops
                    cta: 詳しく見る
                    icon:
                      name: devsecops
                      alt: devsecopsアイコン
                      variant: marketing
                  - title: ソフトウェアの迅速な開発とデプロイ
                    description: GitLabを使用することで、サイクルタイムを短縮させ、より少ない労力でより頻繁にデプロイできます。新機能の計画から自動化テスト、リリースオーケストレーションまで、すべてを単一のアプリケーションで行えます。
                    href: /platform/
                    data_ga_name: platform
                    cta: 詳しく見る
                    icon:
                      name: speed-gauge
                      alt: speed-gaugeアイコン
                      variant: marketing
                  - title: 強力なコラボレーション
                    description: 組織内のサイロを解消し、開発、セキュリティ、運用チームからビジネスチームや非技術系のステークホルダーまで、社内のすべての人がコードを使ってコラボレーションできるようになります。DevSecOpsプラットフォームを使用することで、ライフサイクル全体にわたって可視性を高められ、ソフトウェアプロジェクトの推進に関わる全メンバーでのコミュニケーションを向上できます。
                    icon:
                      name: collaboration-alt-4
                      alt: コラボレーションアイコン
                      variant: marketing
                  - title: オープンソースプラットフォーム
                    description: GitLabはオープンソースのため、世界中の何千人もの開発者が、継続的に改良に取り組むことから得られたイノベーションを活用できます。
                    icon:
                      name: open-source
                      alt: オープンソースアイコン
                      variant: marketing
                  - title: 企業の成長とともにスケールアウト
                    description: 資金調達段階からキャッシュフローが黒字化され、国際的な大企業に成長する段階まで、GitLabは企業成長に合わせて進化しサポートし続けます。
                    href: /customers/
                    data_ga_name: customers
                    cta: お客様の成功事例を見る
                    icon:
                      name: auto-scale
                      alt: オートスケールアイコン
                      variant: marketing
        - name: 'div'
          id: customers
          slot_enabled: true
          slot_content:
            - name: 'education-case-study-carousel'
              data:
                header: GitLabとスタートアップの成功ストーリー
                customers_cta: true
                cta:
                  href: '/customers/'
                  text: 事例をさらに見る
                  data_ga_name: customers
                case_studies:
                  - logo_url: "/nuxt-images/logos/chorus-color.svg"
                    institution_name: Chorus
                    quote:
                      img_url: /nuxt-images/blogimages/Chorus_case_study.png
                      quote_text: GitLabを使用すれば、プロダクトエンジニアリングが容易になり、プロダクトエンジニアリングにおけるコミュニケーションを格段に取りやすくなります。
                      author: Russell Levy氏
                      author_title: 共同創設者兼CTO　Chorus.ai
                    case_study_url: /customers/chorus/
                    data_ga_name: chorus case study
                    data_ga_location: case study carousel
                  - logo_url:  "/nuxt-images/logos/hackerone-logo.png"
                    institution_name: hackerone
                    quote:
                      img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
                      quote_text: GitLabはセキュリティ上の欠陥を早期に発見し、開発者の作業手順に組み込んでいます。 エンジニアはコードをGitLab CIにプッシュし、多くの段階的な監査ステップの1つから即座にフィードバックを得て、そこにセキュリティの脆弱性が組み込まれているかどうかを確認します。さらには特定のセキュリティ問題に特化したテストを行う、独自の新しいステップの構築も可能です。
                      author: Mitch Trale氏
                      author_title: インフラストラクチャ主任　HackerOne
                    case_study_url: /customers/hackerone/
                    data_ga_name: hackerone case study
                    data_ga_location: case study carousel
                  - logo_url: /nuxt-images/logos/anchormen-logo.svg
                    institution_name: Anchormen
                    quote:
                      img_url: /nuxt-images/blogimages/anchormen.jpg
                      quote_text: 真に仕事に集中できます。 GitLabをセットアップしたら、他のことを考える必要がありません。 GitLabが保護してくれるので、自分がやるべき仕事のみに焦点を当てられます。 間違いなく運用効率が向上できます。
                      author: Jeroen Vlek氏
                      author_title: CTO　Anchormen
                    case_study_url: /customers/anchormen/
                    data_ga_name: anchormen case study
                    data_ga_location: case study carousel


    - name: cta-block
      data:
        cards:
          - header: 「スタートアップ向けのGitLabプログラム」があなたの企業に最適か確認してみましょう。
            icon: increase
            link:
              text: スタートアップ向けのGitLabプログラム
              url: /solutions/startups/join/
              data_ga_name: startup join
          - header: ツールチェーンにどれだけコストをかけていますか？
            icon: piggy-bank-alt
            link:
              text: ROI計算ツールを試す
              url: /calculator/roi/
              data_ga_name: roi calculator
